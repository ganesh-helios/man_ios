//
//  forgotPasswordVC.swift
//  ManApp
//
//  Created by Mansi on 08/01/18.
//  Copyright © 2018 Helios Solutions. All rights reserved.
//

import UIKit

class forgotPasswordVC: BaseViewController,UITextFieldDelegate {
    @IBOutlet weak var txtFirstName: EdgedTextFiled!
    @IBOutlet weak var txtLastName: EdgedTextFiled!
    @IBOutlet weak var txtMobile: EdgedTextFiled!
    @IBOutlet weak var txtEmail: EdgedTextFiled!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupView()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setupView(){
        self.txtMobile.inputAccessoryView = self.addToolBar()
        navigationController?.setToolbarHidden(true, animated: true)
    }
    //MARK:- validateForm
    // func use for validate form data
    func validateForm()->Bool{
        if(self.txtFirstName.text!.isBlank) {
            self.showAlert(TR_MSG_ENTER_FIRST_NAME)
            return false
        }
        if(self.txtLastName.text!.isBlank) {
            self.showAlert(TR_MSG_ENTER_LAST_NAME)
            return false
        }
        
        if(self.txtMobile.text!.isBlank) {
            self.showAlert(TR_MSG_ENTER_MOBILE)
            return false
        }
        return true
    }

    @IBAction func Btn_Done_Tapped(_ sender: Any) {
        if(self.validateForm() == false) {
            return
        }
        if(!(self.txtMobile.text!.isPhoneNumber)) {
            self.showAlert(TR_MSG_VALID_MOBILE)
            return
        }
        if(self.txtEmail.text!.isBlank == false) {
            if(!self.txtEmail.text!.isEmail) {
                self.showAlert(TR_MSG_VALID_EMAIL)
                return
            }
        }
        self.sendDetailsToServer()
        print("----DONE-----")
    }
    func sendDetailsToServer() {
        
        let FirstName = self.txtFirstName.text!
        let LastName = self.txtLastName.text!
        let Mobile = self.txtMobile.text!
        let Email = self.txtEmail.text!
        
        let userinfo:String = ["FirstName"," : ",FirstName,",LastName"," : ",LastName,",Mobile"," : ",Mobile].joined()
        
        let dataToSend:[String:String] = ["from":Email,
                                          "user" : userinfo]
        self.view.endEditing(true)
        self.showProgress()
        WebApiController.sharedInstance.forgotPasswordAPI(paramDict: dataToSend as NSDictionary , onSuccess: { (responseObject:AnyObject? ) -> Void in
            self.hideProgress()
            if let returnData:String = responseObject as? String {
                if(returnData.lowercased().contains("success")){
                   self.showAlertWithComplitionSingleOption("Your request is successful.", handler: { (action) in
                        self.dismiss(animated: true, completion: nil)
                   })
                }
            }
        }) { (error:NSError?) -> Void in
            self.hideProgress()
            
            if(error?.code == ERROR_CODE_USER_CANCELLED) {
                return
            }
            
            if(error?.code == -1202) {
                self.showAlert((error?.localizedDescription)!)
                return
            }
            self.showAlertError(error: error!)
        }
    }
    
    
    @IBAction func btn_Cancel_Tapped(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
    
}
