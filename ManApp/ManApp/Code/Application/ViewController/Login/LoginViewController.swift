//
//  LoginViewController.swift
//  ManApp
//
//  Created by Ganesh on 31/10/17.
//  Copyright © 2017 Helios Solutions. All rights reserved.
//

import UIKit
import Alamofire
class LoginViewController: BaseViewController,UITextFieldDelegate {
    @IBOutlet weak var btnLogin: UIButton!
    @IBOutlet weak var txtAccount: EdgedTextFiled!
    @IBOutlet weak var txtUserName: EdgedTextFiled!
    @IBOutlet weak var txtPassword: EdgedTextFiled!
    var countryListRequest:Request?
    @IBOutlet var btnShowHidePassword: UIButton!
    @IBOutlet var imageView: UIImageView!
    var clientDataLimitRequest:Request?
    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationController!.hidesBarsOnSwipe = true
        self.setupView()
//        txtAccount.delegate = self
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
        self.navigationController!.hidesBarsOnSwipe = false
        navigationController?.setToolbarHidden(true, animated: true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    // MARK: - Custom Methode
    // func use for initial setup of view
    func setupView(){
        imageView.isUserInteractionEnabled = true
        self.getCountryList()
//        self.txtAccount.text = "MANT00226"//self.txtAccount.text
//        self.txtUserName.text = "MILLFREIGHT" //"GUARDIAN 1"//"MILLFREIGHT" //self.txtUserName.text
//        self.txtPassword.text = "Mantgs.01"//self.txtPassword.text
        txtPassword.rightViewMode = .always
        txtPassword.rightView = btnShowHidePassword
        
    }
    @IBAction func btnShowHidePassword_TouchDidEndorExit(_ sender: UIButton) {
        print("TouchDidEndorExit")
    }
    @IBAction func btnShowHidePassword_TouchCancel(_ sender: UIButton) {
        self.txtPassword.isSecureTextEntry = true
        print("Touch Cancel")
    }
    @IBAction func btnShowHidePassword_TouchDown(_ sender: UIButton) {
        print("Touch down")
        self.txtPassword.resignFirstResponder()
        self.txtPassword.isSecureTextEntry = false
    }
    
    @IBAction func btnShowHidePassword_TouchUpInside(_ sender: UIButton) {
        self.txtPassword.isSecureTextEntry = true
        print("Touch upinside")
    }
    
    //MARK:- GET COUNTRY
    // func use for getting country list
    func getCountryList(){
        if(Utility.checkInternet() == false){
            return
        }
        if (countryListRequest != nil) {
            return
        }
        self.showProgress()
        self.countryListRequest = WebApiController.sharedInstance.getCountryList([String:String]() as NSDictionary, onSuccess: { (response:AnyObject?) -> Void in
            let responseDict:NSDictionary = response as! NSDictionary
            print(responseDict)
            self.processCountryResponse(responseDict)
            self.countryListRequest = nil;
            self.hideProgress()
        }){ (error: NSError?) -> Void in
            
            self.hideProgress()
            self.countryListRequest = nil;
            
            if(error?.code == ERROR_CODE_USER_CANCELLED) {
                return
            }
            self.showAlertWithComplitionSingleOption(TR_MSG_NO_COUNTRY_FOUND, handler: { (action:UIAlertAction) -> Void in
                self.getCountryList()
            })
            
        }
    }
    
    //MARK:- Process Get Country
    // func use for process country list response
    func processCountryResponse(_ response:NSDictionary) {
        if let arrCountry:NSArray = response.value(forKey: KEY_RESPONSE_COUNTRIES) as? NSArray{
            
            for dic in arrCountry{
                let dicCountry:NSDictionary = dic as! NSDictionary
                let strCountryName:String = dicCountry.value(forKey: KEY_RESPONSE_COUNTRY_NAME) as! String
                if(strCountryName.lowercased() == "south africa"){
                    let strUrl:String = ["https://",dicCountry.value(forKey: KEY_SERVER_URL) as! String].joined()
                    Utility.userDefaultsSetValue(value: strUrl as AnyObject, key: KEY_SERVER_URL)
                    Utility.userDefaultsSetValue(value: strUrl as AnyObject, key: KEY_COUNTRY_URL)
                    Utility.userDefaultsSetValue(value: (dicCountry[KEY_USE_MATRIC] as AnyObject), key: KEY_USE_MATRIC)
                }
            }
        }
    }
    
    //MARK:- validateForm
    // func use for validate form data
    func validateForm()->Bool{
        if(self.txtAccount.text!.isBlank) {
            self.showAlert(TR_MSG_ENTER_ACCOUNT_NAME)
            return false
        }
        if(self.txtUserName.text!.isBlank) {
            self.showAlert(TR_MSG_ENTER_USER_NAME)
            return false
        }
        
        if(self.txtPassword.text!.isBlank) {
            self.showAlert(TR_MSG_ENTER_PASSWORD)
            return false
        }
        return true
    }
    
    // MARK: - button click event
    @IBAction func btnLoginTapped(_ sender: Any) {
        if(self.validateForm() == false) {
            return
        }
        if(Utility.checkInternet() == false) {
            self.showAlert(TR_LBL_DIALOG_CONNECTION_MSG)
            return
        }
        self.callAPILoginToServer()
    }
    
    @IBAction func btnForgotPasswordTapped(_ sender: UIButton) {
        self.performSegue(withIdentifier: "sgShowForgotPassword", sender: nil)
    }
    
    // MARK: - button click event
    // func use make login api call to server
    func callAPILoginToServer() {
        let userName = self.txtAccount.text!
        let subUserName = self.txtUserName.text!
        let password = self.txtPassword.text!
        
        let encrypt : Encrypt = Encrypt()
        
        // Get the username and remove any whitespace
        // Custom function to remove white spaces utils.condenseWhitespace
        let usernameTrimmed = userName.condenseWhitespace()//self.condenseWhitespace(userName)
        
        // Get the password and remove any whitespace
        var passwordTrimmed:NSString = password.condenseWhitespace() as String as NSString//utils.condenseWhitespace(password)
        
        var i = 0
        var newPassString:String = ""
        while i < passwordTrimmed.length {
            let val = passwordTrimmed.character(at: i) & 0xFF
            newPassString.append(Character.init(UnicodeScalar.init(val)!))
            i = i + 1
        }
        
        passwordTrimmed = newPassString as NSString
        // SHA1 the password
        let passwordHash = encrypt.sha1(passwordTrimmed as String) as String// passwordTrimmed.sha1()
        
        // Get the subusername and remove any whitespace
        // In case subuser does not exists. Set subuserTrimmed to “”
        let subuserTrimmed = subUserName    //utils.condenseWhitespace(subUserName)
        
        // Generate the nonce – This is a random integer value from 0 to 999999999 - 1
        let nonce = String(arc4random() % 999999999)
        
        // Generate the current timestamp in the format "yyyy-MM-dd'T'HH:mm:ss'Z'"
        let dateFormatter = DateFormatter.init()
        dateFormatter.dateFormat = DATE_FORMAT_ENCRYPT
        dateFormatter.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale!
        let created = dateFormatter.string(from: Date())
        
        // Create the password digest data by combining the nonce, created timestamp, and hashed password
        let passwordDigestData = "\(nonce)\(created)\(passwordHash)"
        let passwordDigest = encrypt.hash(toBase64: passwordDigestData)
        let nonceDigest = encrypt.encodeBase64(with: nonce)
        
        
        let src_id = 20//22
//        if (UIDevice.currentDevice().userInterfaceIdiom == .Pad) {
//            src_id = 21
//        } else if(UIDevice.currentDevice().userInterfaceIdiom == .Phone) {
//            src_id = 20
//        }
        
        let srsIdis = NSString(format: "%d",src_id)
        
        let dataToSend:[String:String] = [KEY_REQUEST_USER_NAME:usernameTrimmed,
                                          KEY_REQUEST_CLIENT_USER_NAME : subuserTrimmed,
                                          KEY_REQUEST_NOUNCE : nonceDigest!,
                                          KEY_REQUEST_CREATED : created,
                                          KEY_REQUEST_PASSWORD : passwordDigest!,
                                          KEY_REQUEST_FORCE_ROAD_SPEED : "false",
                                          KEY_REQUEST_CLIENT_SRC_ID : srsIdis as String,
                                          KEY_REQUEST_LOCALE : "en_ZA"//NSLocale.current.identifier
        ]
        
        print("DATA Tosend : \(dataToSend)")
        
        self.showProgress()
        WebApiController.sharedInstance.loginWithEmailPassword(paramDict: dataToSend as NSDictionary , onSuccess: { (responseObject:AnyObject? ) -> Void in
            
//            self.hideProgress()
            self.processLoginResponse(responseObject as! NSDictionary)
            
        }) { (error:NSError?) -> Void in
            self.hideProgress()
            
            if(error?.code == ERROR_CODE_USER_CANCELLED) {
                return
            }
            
            if(error?.code == -1202) {
                self.showAlert((error?.localizedDescription)!)
                return
            }
            self.showAlertError(error: error!)
        }
    }
    
    //MARK:- processLoginResponse
    // func use for process login response
    func processLoginResponse(_ response:NSDictionary) {
        let userDict:NSDictionary = response
        if((userDict.value(forKey: KEY_RESPONSE_ERRORS) as! Int == 0)) {
            self.appDelegate.loggedUser = userDict;
            
            Utility.userDefaultsSetValue(value: userDict.value(forKey: KEY_RESPONSE_USER_ID)! as AnyObject, key:KEY_RESPONSE_USER_ID)
            Utility.userDefaultsSetValue(value: userDict.value(forKey:KEY_RESPONSE_USER_NAME)! as AnyObject, key: KEY_RESPONSE_USER_NAME)
            Utility.userDefaultsSetValue(value: userDict.value(forKey:KEY_RESPONSE_SERVER_DOMAIN)! as AnyObject, key: KEY_RESPONSE_SERVER_DOMAIN)
            
            Utility.userDefaultsSetValue(value: userDict.value(forKey:KEY_RESPONSE_CLIENT_USER_ID)! as AnyObject, key: KEY_RESPONSE_CLIENT_USER_ID)
            Utility.userDefaultsSetValue(value: userDict.value(forKey:KEY_RESPONSE_CLIENT_USER_NAME)! as AnyObject, key: KEY_RESPONSE_CLIENT_USER_NAME)
            var fullURL:NSString = userDict.value(forKey:KEY_RESPONSE_SERVER_DOMAIN) as! NSString
            if (fullURL.contains("https:") == false) {
                let prefix:NSString = "https://"
                fullURL = prefix.appending(fullURL as String) as NSString
            }
            fullURL = fullURL.trimmingCharacters(in: NSCharacterSet(charactersIn: " ") as CharacterSet) as NSString
            print("FULLLL : \(fullURL)")
            
            Utility.userDefaultsSetValue(value: fullURL, key: KEY_SERVER_URL)
            
//            self.performSegue(withIdentifier: "idHome", sender: nil)
            self.getClientDataLimit()
        } else {
            self.hideProgress()
            self.showAlert(TR_LBL_INVALID_CREDENTIALS)
        }
    }
    
    // MARK: - textField Delegate
    func textFieldShouldReturn(_ textField: UITextField) -> Bool{
        textField.resignFirstResponder()
        return true
    }
 
    // MARK: - getClientDataLimit
    func getClientDataLimit() {
        if (self.clientDataLimitRequest != nil) {
            return
        }
        
        var userIdParam:NSString = ((appDelegate.loggedUser?.value(forKey: KEY_RESPONSE_USER_ID)! as AnyObject).stringValue! as NSString)
        if let clientID:NSString = appDelegate.loggedUser?.value(forKey: KEY_RESPONSE_CLIENT_USER_ID) as? NSString {
            userIdParam = userIdParam.appendingFormat(":%@", clientID)
        }
        let paramDict:NSDictionary = [KEY_REQUEST_USER_ID:userIdParam];
        
        self.clientDataLimitRequest = WebApiController.sharedInstance.getClientDataLimit(paramDict, onSuccess: { (response:AnyObject?) -> Void in
            self.clientDataLimitRequest = nil
            self.processClientDataResponse(response!)
        }) { (error:NSError?) -> Void in
            self.hideProgress()
            self.clientDataLimitRequest = nil
            if(error?.code == ERROR_CODE_USER_CANCELLED) {
                return
            }
            self.showAlertErrorWithCompletion(error!, handler: { (action:UIAlertAction) -> Void in
                self.showProgress()
                self.getClientDataLimit()
            })
            
        }
    }
    
    // MARK: - processClientDataResponse
    func processClientDataResponse(_ response:AnyObject){
        self.hideProgress()
        let dictLimit: NSDictionary = (response as? NSDictionary)!;
        let arrLimit: NSArray = dictLimit.value(forKey: KEY_RESPONSE_TRIP_LIMIT_LIST) as! NSArray;
        if(arrLimit.count>0){
            let inDict : NSDictionary = arrLimit.object(at: 0) as! NSDictionary;
            let monthValue: NSNumber = inDict.value(forKey: KEY_RESPONSE_DATA_LIMIT_MONTHS) as! NSNumber;
            Utility.userDefaultsSetValue(value: monthValue,key: (KEY_RESPONSE_DATA_LIMIT));
        }
        self.performSegue(withIdentifier: "idHome", sender: nil)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
