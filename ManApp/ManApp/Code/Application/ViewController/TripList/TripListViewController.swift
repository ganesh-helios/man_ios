//
//  TripListViewController.swift
//  ManApp
//
//  Created by Tejas Thanki on 05/01/18.
//  Copyright © 2018 Helios Solutions. All rights reserved.
//

import UIKit
import Alamofire
import DZNEmptyDataSet
class TripListViewController: BaseViewController,UITableViewDelegate,UITableViewDataSource,DZNEmptyDataSetSource,DZNEmptyDataSetDelegate,UITextFieldDelegate {
    var tableArray:NSArray? = NSArray()
    var vehicle:FleetMap?
    var tripPointArray = [TripPoints]()
    
    var tripListRequest:Request?
    let Util = Utility()
    var queue = DispatchQueue(label: "com.cartrack.man", attributes: []);
    var isSelecting:Bool = false
    @IBOutlet var tableView: UITableView!
    @IBOutlet var datePicker:UIDatePicker!
    @IBOutlet var txtHiddenText: UITextField!
    var righButton:UIBarButtonItem!
    @IBOutlet weak var viewCurrentPostion: CardView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.backBarButtonItem?.title = ""
        self.navigationController?.isNavigationBarHidden = false
        self.setupView()
        // Do any additional setup after loading the view.
    }

    override func viewWillDisappear(_ animated: Bool) {
         self.title = ""
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setToolbarHidden(true, animated: true)
        self.title = TR_TRIP_LIST
    }
    // MARK: - setupView
    // func use for intial set up of view
    @objc func setupView(){
        self.title = TR_TRIP_LIST
        tableView.delegate = self
        tableView.dataSource = self
        tableView.tableFooterView = UIView(frame: CGRect.zero)
        let tabCurrentPosition = UITapGestureRecognizer(target: self, action: #selector(self.tabCurrentPosition(_:)))
        viewCurrentPostion.addGestureRecognizer(tabCurrentPosition)
        
        // setup Date Picker Bar Buttons
        var date:Date = Date.init()
        if let strTime:String = self.vehicle?.time {
            date = Utility.dateFromStringAPIDate(strTime)
        }
        self.datePicker.date = date
        righButton = UIBarButtonItem(title: Utility.stringfromDate(date), style: UIBarButtonItemStyle.plain, target: self, action: #selector(TripListViewController.showDatePicker))
        
        let btnLeft: UIButton = UIButton.init(type: UIButtonType.custom)
        // cm
        btnLeft.tintColor = UIColor.white
        
        //set image for button
        
        btnLeft.setImage(UIImage(named: "white_left"), for: UIControlState()) // cm was ic_action_navigation_left
        
        //add function for button
        
        btnLeft.addTarget(self, action: #selector(TripListViewController.selectPreviousDate), for: UIControlEvents.touchUpInside)
        //set frame
        btnLeft.frame = CGRect(x: 0, y: 0, width: 44, height: 44)
        
        let barButtonLeft = UIBarButtonItem(customView: btnLeft)
        
        let btnRight: UIButton = UIButton.init(type: UIButtonType.custom)
        //set image for button
        
        btnRight.setImage(UIImage(named: "white_right"), for: UIControlState())
        
        //add function for button
        btnRight.addTarget(self, action: #selector(TripListViewController.selectNextDate), for: UIControlEvents.touchUpInside)
        //set frame
        btnRight.frame = CGRect(x: 0, y: 0, width: 44, height: 44)
        
        let barButtonRight = UIBarButtonItem(customView: btnRight)
        btnLeft.tintColor = UIColor.init(hex: COLOR_FFFFFF)   
        btnRight.tintColor = UIColor.init(hex: COLOR_FFFFFF)
        righButton.tintColor = UIColor.init(hex: COLOR_FFFFFF)
        righButton.setTitleTextAttributes([NSAttributedStringKey.font: UIFont(name: "Roboto-Bold", size: 15)!,NSAttributedStringKey.foregroundColor : UIColor.init(hex: COLOR_FFFFFF)], for: UIControlState())
        txtHiddenText.inputAccessoryView = self.addToolBar()
        navigationItem.rightBarButtonItems = [barButtonRight,righButton,barButtonLeft]
        
        txtHiddenText.inputView = self.datePicker
        
        self.tableView.addSubview(self.refreshControl)
        
        datePicker.maximumDate = Date.init()
        
        let clientDataLimit : NSNumber = Utility.userDefaultsGetValue(key: KEY_RESPONSE_DATA_LIMIT) as! NSNumber;
        //let clientDataLimit : NSNumber = 1
        var dataLimit: Double = clientDataLimit.doubleValue;
        dataLimit = dataLimit * 30;
        datePicker.minimumDate = Date.init(timeInterval: -(dataLimit*24*60*60), since: Date.init())
        self.getDataFromServer()
        self.tableView.estimatedRowHeight = 500.0
        self.tableView.rowHeight = UITableViewAutomaticDimension
        
        self.navigationController?.navigationBar.backItem?.title = " "
        
  
    
    }
    
    // MARK: - tabCurrentPosition
    @objc func tabCurrentPosition(_ sender: UITapGestureRecognizer) {
        self.performSegue(withIdentifier: "sgShowCurrentPositionView", sender: nil)
    }
    
    // MARK: - selectPreviousDate
    @objc func selectPreviousDate() {
        if (txtHiddenText.isFirstResponder == true) {
            txtHiddenText.resignFirstResponder()
        }
        var date:Date = datePicker.date;
        
        date = date.addingTimeInterval(60*60*24*(-1));
        if(date.timeIntervalSince1970 < (datePicker.minimumDate?.timeIntervalSince1970)!) {
            return;
        }
        
        datePicker.date = date;
        righButton.title = Utility.stringfromDate(date)
        self.getDataFromServer()
        
    }
    
    // MARK: - selectNextDate
    @objc func selectNextDate() {
        if (txtHiddenText.isFirstResponder == true) {
            txtHiddenText.resignFirstResponder()
        }
        
        var date:Date = datePicker.date;
        
        date = date.addingTimeInterval(60*60*24);
        if(date.timeIntervalSince1970 > (datePicker.maximumDate?.timeIntervalSince1970)!) {
            return;
        }
        datePicker.date = date;
        
        righButton.title = Utility.stringfromDate(date)
        self.getDataFromServer()
    }
    
    // MARK: - showDatePicker
    @objc func showDatePicker() {
        txtHiddenText.becomeFirstResponder()
    }
    
    
    // MARK: - Get Data From Server
    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        // Do some reloading of data and update the table view's data source
        // Fetch more objects from a web service, for example...
        
        self.getDataFromServer()
    }
    
    // MARK: - refreshControl
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.tintColor = UIColor.white
        
        refreshControl.addTarget(self, action: #selector(TripListViewController.handleRefresh(_:)), for: UIControlEvents.valueChanged)
        
        return refreshControl
    }()
    
    // MARK: - getDataFromServer
    func getDataFromServer() {
        // block concurrent request
        if (self.tripListRequest != nil) {
            return
        }
        
        self.showProgress()
        var userIdParam:String = (appDelegate.loggedUser?.value(forKey: KEY_RESPONSE_USER_ID)! as AnyObject).stringValue
        
        if let clientID:NSString = appDelegate.loggedUser?.value(forKey: KEY_RESPONSE_CLIENT_USER_ID) as? NSString {
            userIdParam = userIdParam.appendingFormat(":%@", clientID)
        }
        
        let formatter:DateFormatter = DateFormatter.init()
        formatter.dateFormat = DATE_FORMAT_DATE
        formatter.timeZone = TimeZone.autoupdatingCurrent
        formatter.locale =  Locale(identifier: "en_US_POSIX")
        
        
        let date:Date = formatter.date(from: righButton.title! as String)!//Utility.dateFromStringDate(righButton.title!)
        
        let sdate:NSString = NSString.init(format: "%.0f000", date.timeIntervalSince1970)
        let edate:NSString = NSString.init(format: "%.0f000", (date.timeIntervalSince1970 + 24*3600) - 1)
        //let edate:NSString = NSString.init(format: "%.0f000", date.timeIntervalSince1970)
        
        let paramDict:NSDictionary = [KEY_REQUEST_USER_ID:userIdParam,
                                      KEY_REQUEST_VEHICLE_ID:(self.vehicle?.vehicleId)!,
                                      KEY_REQUEST_TRIP_START_DATE:sdate,
                                      KEY_REQUEST_TRIP_END_DATE:edate];
        
        self.tripListRequest = WebApiController.sharedInstance.getTripList(paramDict, onSuccess: { (response:AnyObject?) -> Void in
            self.refreshControl.endRefreshing()
            self.hideProgress()
            self.processTripListResponse(response!)
            self.tripListRequest = nil
            
        }) { (error:NSError?) -> Void in
            self.tripListRequest = nil
            self.refreshControl.endRefreshing()
            
            self.hideProgress()
            
            if(error?.code == ERROR_CODE_USER_CANCELLED) {
                return
            }
            self.showAlertError(error: error!)
        }
    }
    
    // MARK: - processTripListResponse
    func processTripListResponse(_ response:AnyObject) {
        
        let results:[NSDictionary] = response as! [NSDictionary]
        
//        self.tableArray = results.sorted { (trip1: NSDictionary, trip2: NSDictionary) -> Bool in
//
//            let dateStart1:Date = Utility.dateFromStringAPIDate(trip1.value(forKey: KEY_RESPONSE_TRIP_MAP_START_TS) as! String)
//            let dateStart2:Date = Utility.dateFromStringAPIDate(trip2.value(forKey: KEY_RESPONSE_TRIP_MAP_START_TS) as! String)
//
//            return dateStart1.timeIntervalSinceNow > dateStart2.timeIntervalSinceNow
//            } as NSArray
        self.tableArray = results as NSArray
        tableView.reloadData()
    }
    
    // MARK: - TableView DataSource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (self.tableArray?.count)!
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cellIndentifier:String = "tripList"
        
        let cell:TripTableViewCell = tableView.dequeueReusableCell(withIdentifier: cellIndentifier) as! TripTableViewCell
        
        let cellDict:NSDictionary = (self.tableArray?.object(at: (indexPath as NSIndexPath).row))! as! NSDictionary
        
        cell.setupCell(cellDict)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        if (txtHiddenText.isFirstResponder == true) {
            txtHiddenText.resignFirstResponder()
        }
        if(Utility.checkInternet()){
            self.getVehicleTrip(indexPath)
        }else{
            self.showAlert(TR_LBL_DIALOG_CONNECTION_MSG)
        }
        
    }
    
    // MARK: - EmptyDataSet Delegate
    func image(forEmptyDataSet scrollView: UIScrollView!) -> UIImage! {
        return #imageLiteral(resourceName: "no-data")
    }
    
    func description(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
        return NSAttributedString.init(string: TR_MSG_NO_TRIP_AVAILABLE)
    }
    
    // MARK: - getVehicleTrip
    func getVehicleTrip(_ indexPath: IndexPath) {
        
        let vehicleTrip:NSDictionary = (self.tableArray?.object(at: (indexPath as NSIndexPath).row))! as! NSDictionary
        
        self.showProgress()
        var userIdParam:String = ((appDelegate.loggedUser?.value(forKey: KEY_RESPONSE_USER_ID)! as AnyObject).stringValue)
        
        if let clientID:NSString = appDelegate.loggedUser?.value(forKey: KEY_RESPONSE_CLIENT_USER_ID) as? NSString {
            userIdParam = userIdParam.appendingFormat(":%@", clientID)
        }
        
        let today:String = Utility.stringfromDate(Date.init())
        let selectedDate:String = righButton.title!
        
        if ((indexPath as NSIndexPath).row == 0 && self.vehicle?.ignition == 2 && today == selectedDate) {
            
            let paramDict:NSDictionary = [KEY_REQUEST_USER_ID:userIdParam,
                                          KEY_REQUEST_VEHICLE_ID:(self.vehicle?.vehicleId)!];
            
            _ = WebApiController.sharedInstance.getCurrentVehicleTrip(paramDict, onSuccess: { (response:AnyObject?) -> Void in
                self.hideProgress()
                self.processVehicleTripResponse(response!)
            }) { (error:NSError?) -> Void in
                self.showAlertError(error: error!)
                self.hideProgress()
            }
        } else {
            
            let paramDict:NSDictionary = [KEY_REQUEST_USER_ID:userIdParam,
                                          KEY_REQUEST_VEHICLE_ID:(self.vehicle?.vehicleId)!,
                                          KEY_REQUEST_TRIP_MAP_START_TS:(vehicleTrip.value(forKey: KEY_RESPONSE_TRIP_MAP_START_TS) as? NSString)!,
                                          KEY_REQUEST_TRIP_MAP_END_TS:(vehicleTrip.value(forKey: KEY_RESPONSE_TRIP_MAP_END_TS) as? NSString)!];
            
           _ = WebApiController.sharedInstance.getVehicleTrip(paramDict, onSuccess: { (response:AnyObject?) -> Void in
                self.hideProgress()
                self.processVehicleTripResponse(response!)
            }) { (error:NSError?) -> Void in
                self.hideProgress()
                
                if(error?.code == ERROR_CODE_USER_CANCELLED) {
                    return
                }
                
                self.showAlertError(error: error!)
            }
        }
    }
    
    // MARK: - processVehicleTripResponse
    func processVehicleTripResponse(_ response:AnyObject) {
        let vehicleResponse:NSArray = response as! NSArray
        
        
        if(vehicleResponse.count != 2) {
            self.showAlert(TR_TRIP_LOAD_POSITION)
            return
        }
        let tripPoints:NSArray = vehicleResponse.object(at: 1) as! NSArray
        tripPointArray.removeAll()
        if (tripPoints.count == 0) {
            self.showAlert(TR_TRIP_LOAD_POSITION)
            return;
        }
        
        for tripPoint in tripPoints
        {
            let mTripPoint = TripPoints.init(param: tripPoint as! NSDictionary)
            tripPointArray.append(mTripPoint)
        }
        
        self.performSegue(withIdentifier: "sgShowTripMap", sender: nil);
        
    }
    
    // MARK: - textfield Delegate
    func textFieldDidEndEditing(_ textField: UITextField) {
        if(righButton.title == Utility.stringfromDate(datePicker.date)) {
            return
        }
        
        righButton.title = Utility.stringfromDate(datePicker.date)
        self.getDataFromServer()
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "sgShowCurrentPositionView"{
            if let currentPosition:CurrentPositionViewController = segue.destination as? CurrentPositionViewController {
                currentPosition.vehicle = self.vehicle
            }
        }else if  segue.identifier == "sgShowTripMap"{
            if let tripMapView:TripMapViewController = segue.destination as? TripMapViewController {
                // Get the new view controller using segue.destinationViewController.
                // Pass the selected object to the new view controller.
                
                
                //NB: this data is from two different APIs depending wheather the use selected the current trip or not.
                //normal trip gets its data from '/service/svc2.php/clients/{user_id}/vehicles/{vehicle_id}/trips/select/...'
                //where as Current trip data is from '/service/svc2.php/clients/{user_id}/vehicles/{vehicle_id}/trips/current'
                
                //prepare array for remove all adjacent duplicate points
                var newTripPointArray = [TripPoints]()
                for index in 0...(tripPointArray.count - 1) {
                    if(index != (tripPointArray.count - 1)){
                        let currentTripPont:TripPoints = tripPointArray[index]
                        let nextTripPont:TripPoints = tripPointArray[index + 1]
                        if(currentTripPont.latitude != nextTripPont.latitude && currentTripPont.longitude != nextTripPont.longitude){
                                newTripPointArray.append(currentTripPont)
                        }
                    }else{
                        //last point
                        newTripPointArray.append(tripPointArray[index])
                    }
                }
                //load Mock Data
                tripMapView.points = newTripPointArray//data from API / Core Data depends where you opted to source data to UI
                
                tripMapView.vehicle = self.vehicle
                
                // check if its Current Trip
                if tripMapView.points[tripMapView.points.count - 1].ignition == 2{
                    tripMapView.isCurrentTrip = true
                }
            }
        }
    }
    

    
    

}



