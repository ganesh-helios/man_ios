//
//  TripMapViewController.swift
//  ManApp
//
//  Created by Tejas Thanki on 09/01/18.
//  Copyright © 2018 Helios Solutions. All rights reserved.
//

import UIKit
import Alamofire
import GoogleMaps
class TripMapViewController: BaseViewController {

    let defZoomLevel:UInt = 15
    
    var viewTrigger = "TripList"
    
    var vehicle:FleetMap!
    
    var isCurrentTrip = false // flag to determine if the displayed trip is the current trip
    
    var mapTemplate:Int = 0 //index of map tile type
    
    var points = [TripPoints]() // trip points
    
    var pointPlayCount = 0 // number of points ploted during trip playing
    
    var playTimer:Timer? // Play trip timer/invertval
    
    var playing:Bool = false // flag to determine if trip is currently playing
    
    var autoTimer:Timer = Timer()
    
    var vehicleTripRequest:Request?
    
    var autoTrackVehicle:Bool = true
    
    var refreshBarButton: UIButton?
    
    var userTouches:Bool = false
    
    var tapInterceptor:WildcardGestureRecognizer?
    @IBOutlet weak var googleMapView: GMSMapView!
    //var overlay:MKCartrackTileOverlay?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupView()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        navigationController?.setToolbarHidden(true, animated: true)
        self.autoTimer.invalidate()
        
        if (self.vehicleTripRequest != nil) {
            self.vehicleTripRequest?.cancel()
        }
    }
    
    // MARK: - setupView
    // func use for intial set up of view
    func setupView(){
        self.title = TR_TRIP_MAP
    
        // Toolbar Configuration
        let spacer = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: self, action: nil)
        let startLocation = UIBarButtonItem(image: #imageLiteral(resourceName: "ic_start_location"), style: .plain, target: self, action: #selector(TripMapViewController.goToStartLocation))
        let lastLocation = UIBarButtonItem(image: #imageLiteral(resourceName: "ic_last_location"), style: .plain, target: self, action: #selector(TripMapViewController.goToLastLocation))
        toolbarItems = [spacer,startLocation,lastLocation]
        navigationController?.setToolbarHidden(false, animated: true)
        self.refreshNavigationIcons()
        self.appStaticPointsToMap()
    }

    // MARK: - goToStartLocation
    @objc func goToStartLocation(){
        //After checking if point array is not empty center map to first point
        if points.count > 0 {
            if (points[0].latitude == nil &&
                points[0].longitude == nil){
                return
            } else {
                let camera = GMSCameraPosition.camera(withLatitude: points[0].latitude!, longitude: points[0].longitude!, zoom: 16.0)
                self.googleMapView.animate(to: camera)
             
            }
            
        }
        
    }
    
    // MARK: - goToLastLocation
    @objc func goToLastLocation(){
        //After checking if point array is not empty center map to last point
        if points.count > 0 {
            if (points[points.count - 1].latitude == nil ||
                points[points.count - 1].longitude == nil){
                return
            } else {
                let countIs : Int = points.count;
                let camera = GMSCameraPosition.camera(withLatitude: points[countIs - 1].latitude!, longitude: points[countIs - 1].longitude!, zoom: 16.0)
                self.googleMapView.animate(to: camera)
//                self.googleMapView.camera = camera
            }
        }
    }
    
    //Refresh Navigation Icons
    func refreshNavigationIcons(){
        // navigation Bar
        let btnPlay: UIButton = UIButton.init(type: UIButtonType.custom)
        //set image for button
        btnPlay.setImage(#imageLiteral(resourceName: "play"), for: UIControlState()) //cm ic_play
        btnPlay.addTarget(self, action: #selector(TripMapViewController.playTrip), for: UIControlEvents.touchUpInside)
        //set frame
        btnPlay.frame = CGRect(x: 0, y: 0, width: 25, height: 25)
        let play = UIBarButtonItem(customView: btnPlay)
        
        let btnStop: UIButton = UIButton.init(type: UIButtonType.custom)
        //set image for button
        btnStop.setImage(#imageLiteral(resourceName: "pause"), for: UIControlState()) //cm ic_stop
        btnStop.addTarget(self, action: #selector(TripMapViewController.playTrip), for: UIControlEvents.touchUpInside)
        //set frame
        btnStop.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        
        let stop = UIBarButtonItem(customView: btnStop)
        if(playing) {
            // display stop icon if trip is currently playing
            navigationItem.rightBarButtonItems = [stop]
        } else if(!playing) {
            // display play icon if not current trip and trip is not playing
            navigationItem.rightBarButtonItems = [play]
        }
    }
    
    // MARK: - playTrip
    @objc func playTrip() {
        self.userTouches = false
        // toggle playing of trip
//        if !playing && pointPlayCount == 0 { // checks weather trip is playing
        if !playing  {
            // set play trip flag to true incase user taps icon again
            playing = true
            //self.mapView.addGestureRecognizer(self.tapInterceptor!) //dev comment
            
            self.autoTrackVehicle = false
            
            //remove play option on navigation bar and put stop play trip icon
            self.refreshNavigationIcons()
            if(pointPlayCount == 0){
                // remove all markers on map
                self.removeMarkers()
            }

            //trigger start playing trip in 2 seconds
            let date = Date().addingTimeInterval(2)
            
            //set plot trip interval to the Constant plot rate
            playTimer = Timer(fireAt: date, interval: PLAY_TRIP_INTERVAL, target: self, selector: #selector(TripMapViewController.plotPoints), userInfo: nil, repeats: true)
            RunLoop.main.add(playTimer!, forMode: RunLoopMode.commonModes)
            self.navigationController?.setToolbarHidden(true, animated: true)
            
            UIApplication.shared.isIdleTimerDisabled = false
            UIApplication.shared.isIdleTimerDisabled = true
            self.googleMapView.isUserInteractionEnabled = false
            
        } else {
            // Stop play trip interval
            playTimer!.invalidate()
            
            // reset point plot count to zero
            //pointPlayCount = 0
            
            // reset playing trip flag
            playing = false
            //self.mapView.removeGestureRecognizer(self.tapInterceptor!) //dev commnet
            
            
            // remove already plotted trip markers from map
            //removeMarkers()
            
            
            //remove stop option on navigation bar and put play trip icon
            refreshNavigationIcons()
            
            //replot original trip points
            //appStaticPointsToMap()
//            self.navigationController?.setToolbarHidden(false, animated: true)
            
            UIApplication.shared.isIdleTimerDisabled = true
            UIApplication.shared.isIdleTimerDisabled = false
            self.googleMapView.isUserInteractionEnabled = true
        }
        
    }
    
    // MARK: - plotPoints
    @objc func plotPoints(){
        // plot point on map
        if pointPlayCount < points.count {
            //plot single point on map
            addSinglePoint(pointPlayCount)
//            if (userTouches != true) {
//                self.recentreMap(self.points[self.pointPlayCount].latitude!,lon:self.points[self.pointPlayCount].longitude!,doMapSpan: self.pointPlayCount == 0)
//            } // dev comment
            
            // increment plot count
            pointPlayCount += 1
            UIApplication.shared.isIdleTimerDisabled = false
            UIApplication.shared.isIdleTimerDisabled = true
        }else{
            // Stop play trip interval
            playTimer!.invalidate()
            
            // reset point plot count to zero
            pointPlayCount = 0
            // reset playing trip flag
            playing = false
            //self.mapView.removeGestureRecognizer(self.tapInterceptor!) // dev comment
            
            // remove already plotted trip markers from map
            self.removeMarkers() // dev comment
            
            //remove stop option on navigation bar and put play trip icon
            refreshNavigationIcons()
            
            self.navigationController?.isToolbarHidden = false
            
            UIApplication.shared.isIdleTimerDisabled = true
            UIApplication.shared.isIdleTimerDisabled = false
            
            //replot original trip points
            self.appStaticPointsToMap()
            self.googleMapView.isUserInteractionEnabled = true
        }
    }
    
    
    // MARK: - addSinglePoint
    func addSinglePoint(_ ind:Int,isStatic:Bool?=false){
        //NB: refer to the above comments
        // can be called above in the loop for optimization
        if ind >= points.count {
            return
        }
        //DDLogVerbose("____Response \(JSON)")
//        print("____lat \(points[ind].latitude!) ________long \(points[ind].longitude!)")
        if(isStatic == false){
            // Create a GMSCameraPosition that tells the map to display the
            let camera = GMSCameraPosition.camera(withLatitude: points[ind].latitude!, longitude: points[ind].longitude!, zoom: 16.0)
                self.googleMapView.animate(to: camera)
        }
        // Creates a marker in the center of the map.
        let marker = GMSMarker()
        marker.position = CLLocationCoordinate2D(latitude: points[ind].latitude!, longitude: points[ind].longitude!)
        
        let strOdometer = NSString.init(format:"%@ \(TR_MAP_TITLE_ODOMETER_UNIT)" as NSString, Utility.formattedNumber(((points[ind].odometer?.doubleValue)!/1000.0))) as String
        if ind == 0 {
            marker.icon = #imageLiteral(resourceName: "pin_start")
            marker.title = vehicle.registration
            marker.snippet = [TR_LBL_LOCATION," : ",points[ind].location!,"\n",TR_LBL_TIME," : ", Utility.localTimeOnlyFromString(points[ind].time!),"\n",TR_LBL_SPEED," : ",String(describing: points[ind].speed!)," ",TR_LBL_KMH," ",TR_TRIP_ODOMETER," : ",strOdometer].joined()
            marker.map = self.googleMapView
            
        }else if ind == (points.count - 1) && points[ind].ignition! == 1{
            var image:UIImage = #imageLiteral(resourceName: "ic_vehicle_ign_off")
            image = image.imageRotatedByDegrees(CGFloat(points[ind].bearing!) , flip: false)
            marker.icon = image
            marker.title = vehicle.registration
            marker.snippet = [TR_LBL_LOCATION," : ",points[ind].location!,"\n",TR_LBL_TIME," : ", Utility.localTimeOnlyFromString(points[ind].time!),"\n",TR_LBL_SPEED," : ",String(describing: points[ind].speed!)," ",TR_LBL_KMH," ",TR_TRIP_ODOMETER," : ",strOdometer].joined()
            
        }else if ind == (points.count - 1) && points[ind].ignition! == 2{
            var image:UIImage = #imageLiteral(resourceName: "ic_vehicle_ign_on")
            image = image.imageRotatedByDegrees(CGFloat(points[ind].bearing!) , flip: false)
            marker.icon = image
            marker.title = vehicle.registration
            marker.snippet = [TR_LBL_LOCATION," : ",points[ind].location!,"\n",TR_LBL_TIME," : ", Utility.localTimeOnlyFromString(points[ind].time!),"\n",TR_LBL_SPEED," : ",String(describing: points[ind].speed!)," ",TR_LBL_KMH," ",TR_TRIP_ODOMETER," : ",strOdometer].joined()
        }else{
            var image:UIImage = #imageLiteral(resourceName: "pin")
            image = image.imageRotatedByDegrees(CGFloat(points[ind].bearing!) , flip: false)
            marker.icon = image
            marker.snippet = [TR_LBL_TIME," : ", Utility.localTimeOnlyFromString(points[ind].time!),"\n",TR_LBL_SPEED," : ",String(describing: points[ind].speed!)," ",TR_LBL_KMH].joined()
        }
       
        marker.map = self.googleMapView
    }
    
    // MARK: - removeMarkers
    func removeMarkers(){
        // remove all markers on map
        self.googleMapView.clear()
    }
    
    // MARK: - appStaticPointsToMap
    func appStaticPointsToMap(){
        if(points.count != 0) {
            googleMapView.clear()
        }
        
        if points.count == 0 {
            return
        }
        
       // let path = GMSMutablePath()
        for index in 0...(points.count - 1) {
            self.addSinglePoint(index,isStatic: true)
        }
        
        let camera = GMSCameraPosition.camera(withLatitude: points[0].latitude!, longitude: points[0].longitude!, zoom: 12.0)
        self.googleMapView.animate(to: camera)
 
        /*
        let source : CLLocationCoordinate2D = CLLocationCoordinate2D.init(latitude: points[0].latitude! , longitude: points[0].longitude!);
        let countIs : Int = points.count;
        let destination : CLLocationCoordinate2D = CLLocationCoordinate2D.init(latitude: points[countIs - 1].latitude! , longitude: points[countIs - 1].longitude!);
        
        let bounds = GMSCoordinateBounds(coordinate: source, coordinate: destination);
        let cameraUpdate = GMSCameraUpdate.fit(bounds, with: UIEdgeInsets(top: 60, left: 30, bottom: 60, right: 30))
        self.googleMapView.animate(with: cameraUpdate)
 */
    }
    

    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
