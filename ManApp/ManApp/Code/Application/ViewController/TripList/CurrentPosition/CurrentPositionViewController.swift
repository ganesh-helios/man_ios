//
//  CurrentPositionViewController.swift
//  ManApp
//
//  Created by Tejas Thanki on 08/01/18.
//  Copyright © 2018 Helios Solutions. All rights reserved.
//

import UIKit
import GoogleMaps

class CurrentPositionViewController: BaseViewController,GMSMapViewDelegate {
    var vehicle:FleetMap?
    @IBOutlet weak var lblCurrentVehicle: UILabel!
    @IBOutlet weak var googleMapView: GMSMapView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupView()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - setupView
    // func use for initail setup of view
    func setupView(){
        self.title = TR_TITLE_ACTIVITY_CURRENT_POSITION.capitalized
        self.lblCurrentVehicle.text = [TR_LBL_CURRENT_VEHICLE," : ",String(describing: vehicle!.registration!)].joined()
        self.navigationItem.backBarButtonItem?.title = ""
       self.setupMapView()
    }
    
    func setupMapView(){
        // Create a GMSCameraPosition that tells the map to display the
        let camera = GMSCameraPosition.camera(withLatitude: (vehicle?.latitude)!, longitude: (vehicle?.longitude)!, zoom: 15.0)
        //let mapView = GMSMapView.map(withFrame: CGRect.zero, camera: camera)
        self.googleMapView.camera = camera
        //view = mapView
        
        
        // Creates a marker in the center of the map.
        let marker = GMSMarker()
        marker.position = CLLocationCoordinate2D(latitude: (vehicle?.latitude)!, longitude: (vehicle?.longitude)!)
        marker.icon = (vehicle!.ignition! <= 1) ? #imageLiteral(resourceName: "ic_vehicle_ign_off") : #imageLiteral(resourceName: "ic_vehicle_ign_on")
        marker.title = vehicle?.location
        marker.map = self.googleMapView
    }
    
    //MARK:- +++++Map delegate+++++
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
        self.googleMapView.isMyLocationEnabled = true
    }
    
    func mapView(_ mapView: GMSMapView, willMove gesture: Bool) {
        self.googleMapView.isMyLocationEnabled = true
        if gesture {
            mapView.selectedMarker = nil
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
