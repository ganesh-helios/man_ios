//
//  TripTableViewCell.swift
//  CarTrackFleet
//
//  Created by Diptesh Patel on 23/12/15.
//  Copyright © 2015 Jigar Shah. All rights reserved.
//

import UIKit

class TripTableViewCell: UITableViewCell {
    
    @IBOutlet var lblTitle: UILabel!
    @IBOutlet var lblStartTime: UILabel!
    @IBOutlet var lblStartLocation: UILabel!

    @IBOutlet var lblEndTime: UILabel!
    @IBOutlet var lblEndLocation: UILabel!

    @IBOutlet var lblDuration: UILabel!
    @IBOutlet var lblDistance: UILabel!
    @IBOutlet var lblOdometer: UILabel!
    
    
    @IBOutlet var lblDurationTitle: UILabel!
    @IBOutlet var lblDistanceTitle: UILabel!
    @IBOutlet var lblOdometerTitle: UILabel!
    var useMetric = false
    override func awakeFromNib() {
        super.awakeFromNib()
        
        // Initialization code
        self.lblDurationTitle.text = TR_TRIP_DURATION
        self.lblDistanceTitle.text = TR_TRIP_DISTANCE
        self.lblOdometerTitle.text = TR_TRIP_ODOMETER

        if(Int(Utility.userDefaultsGetStringValue(key: KEY_USE_MATRIC)) == 0){
            useMetric = false
        }else{
            useMetric = true
        }
      
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
        
    }
    
    func setupCell(_ cellDict:NSDictionary) {
        self.backgroundColor = UIColor.clear
        //self.lblStartLocation.sizeToFit()
        //self.lblEndLocation.sizeToFit()
        var tripDist:NSNumber = 0
        if let distance = cellDict[KEY_RESPONSE_TRIP_DISTANCE] as? NSNumber {
            tripDist = distance
        }
        let tripTime:NSString = cellDict.value(forKey: KEY_RESPONSE_TRIP_TIME) as! NSString
        var driver:String = " "
        if let driverName = cellDict[KEY_RESPONSE_TRIP_DRIVER_NAME] as? String {
            driver = NSString.init(format: "%@", driverName) as String
        }

        //self.lblTitle.text = NSString.init(format: "%@",driver) as String
        self.lblStartLocation.text = cellDict.value(forKey: KEY_RESPONSE_TRIP_START_LOCATION) as? String
        self.lblEndLocation.text = cellDict.value(forKey: KEY_RESPONSE_TRIP_END_LOCATION) as? String
        
        
        let formatter:DateFormatter = DateFormatter.init()
        formatter.dateFormat = DATE_FORMAT_API
        formatter.locale =  Locale(identifier: "en_US_POSIX")
        let dateStart:Date = formatter.date(from: cellDict.value(forKey: KEY_RESPONSE_TRIP_MAP_START_TS) as! String)!
        formatter.timeZone = TimeZone.autoupdatingCurrent
        formatter.dateFormat = DATE_FORMAT_TIME
        self.lblStartTime.text = formatter.string(from: dateStart)
        
        formatter.dateFormat = DATE_FORMAT_API
        let endStart:Date = formatter.date(from: cellDict.value(forKey: KEY_RESPONSE_TRIP_MAP_END_TS) as! String)!
        formatter.dateFormat = DATE_FORMAT_TIME

        self.lblEndTime.text = formatter.string(from: endStart)
        self.lblDuration.text = tripTime as String
        
        var endOdometer:NSNumber = 0
        if let endOdo:NSNumber = cellDict["end_odometer"] as? NSNumber {
            endOdometer = NSNumber(value:(endOdo.floatValue/1000))
        }
        
        if useMetric == true {
            self.lblDistance.text = NSString.init(format:"%@ \(TR_MAP_TITLE_ODOMETER_UNIT)" as NSString, Utility.formattedNumber(tripDist.doubleValue/1000)) as String
            self.lblOdometer.text = NSString.init(format:"%@ \(TR_MAP_TITLE_ODOMETER_UNIT)" as NSString, Utility.formattedNumber(endOdometer.doubleValue)) as String
        } else {
            self.lblDistance.text = NSString.init(format:"%@ \(TR_MAP_TITLE_ODOMETER_UNIT_NON_METRIC)" as NSString, Utility.formattedNumber(tripDist.doubleValue/1000*KM_TO_MILE)) as String
            self.lblOdometer.text = NSString.init(format:"%@ \(TR_MAP_TITLE_ODOMETER_UNIT_NON_METRIC)" as NSString, Utility.formattedNumber(endOdometer.doubleValue*KM_TO_MILE)) as String
        }
        //self.lblStartLocation.sizeToFit()
        //self.lblEndLocation.sizeToFit()
        
    }
}

