//
//  HomeViewController.swift
//  ManApp
//
//  Created by Ganesh on 16/10/17.
//  Copyright © 2017 Helios Solutions. All rights reserved.
//

import UIKit
import Alamofire

class HomeViewController: BaseViewController, UITableViewDelegate, UITableViewDataSource,UISearchBarDelegate {

    @IBOutlet weak var tableView: UITableView!
    
//    var tableArray:NSMutableArray? = NSMutableArray()
    var tableArray:[FleetMap] = []
    var groupList:NSArray?
    
    var groupParamArray:NSArray?
    var groupParamDict:NSDictionary?
    
    var responseDict:NSDictionary?
    
    var autoTimer:Timer = Timer()
    var fleetListRequest:Request?
    //var searchBar = UISearchBar()
    var isSearching:Bool = false
    var queue = DispatchQueue.init(label: "com.cartrack.enduser")//dispatch_queue_create("com.cartrack.enduser", nil);
    var fleetListArray:NSArray!
    @IBOutlet weak var cnlblSearchResultHeight: NSLayoutConstraint!
    
    @IBOutlet weak var btnRefresh: UIBarButtonItem!
    @IBOutlet weak var lblSearchResult: UILabel!
    @IBOutlet var viewSearchResult: UIView!
    @IBOutlet weak var searchBar: UISearchBar!
    var isAutoRefresh:Bool = false
    //MARK:- ____DID Load____
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        self.navigationController?.isNavigationBarHidden = false
//        self.navigationController!.navigationBar.isTranslucent = false
        self.navigationController!.navigationBar.shadowImage = #imageLiteral(resourceName: "TransparentPixel")
        // "Pixel" is a solid white 1x1 image.
        self.navigationController!.navigationBar.setBackgroundImage(#imageLiteral(resourceName: "TransparentPixel.png"), for: .default)
//        self.navigationController!.hidesBarsOnSwipe = true
//        self.tableView.style  .plain
        self.tableView.estimatedSectionHeaderHeight = 100
        self.getVehicleListDataFromServer()
        
    }
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController!.hidesBarsOnSwipe = true
        self.navigationController?.isNavigationBarHidden = false
        navigationController?.setToolbarHidden(true, animated: true)
       
        self.setUpView()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController!.hidesBarsOnSwipe = false
        self.title = ""
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - setUpView
    //func is used for intial setup of view
    func setUpView() {
        self.title = Utility.userDefaultsGetStringValue(key: KEY_RESPONSE_USER_NAME)
        self.tableView.separatorColor = self.tableView.backgroundColor
        if(isAutoRefresh == false){
            btnRefresh.image = #imageLiteral(resourceName: "refresh_selected")
        }else {
            btnRefresh.image = #imageLiteral(resourceName: "refresh_red_normal")
        }
    }
    
    //MARK: - button Click events
    @IBAction func btnLogoutClick(_ sender: UIBarButtonItem) {
        let alertController = UIAlertController(title: TR_INFO_TITLE, message:TR_INFO_DESCRIPTION, preferredStyle: UIAlertControllerStyle.alert)
        let okAction = UIAlertAction(title: TR_OK, style: UIAlertActionStyle.default, handler: {
            alert -> Void in
            Utility.userDefaultsSetValue(value: Utility.userDefaultsGetValue(key: KEY_COUNTRY_URL), key: KEY_SERVER_URL)
            self.navigationController?.navigationBar.isHidden = false
            _ = self.navigationController?.popViewController(animated: true)
        })
        let cancelAction = UIAlertAction(title: TR_CANCEL, style: UIAlertActionStyle.default, handler: {
            alert -> Void in
            
        })
        alertController.addAction(okAction)
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    @IBAction func btnAutoRefreshClick(_ sender: UIBarButtonItem) {
        if(isAutoRefresh == false){
            isAutoRefresh = true
            self.setAutoTimer()
            btnRefresh.image = #imageLiteral(resourceName: "refresh_selected")
            btnRefresh.tintColor = UIColor.white
            self.showToast(TR_MSG_AUTO_REFRESH_ACTIVATED)
            
        }else {
            isAutoRefresh = false
            self.autoTimer.invalidate()
            btnRefresh.image = #imageLiteral(resourceName: "refresh_red_normal")
            btnRefresh.tintColor = UIColor.init(hex: "E43035")
            self.showToast(TR_MSG_AUTO_REFRESH_DEACTIVATED)
        }
    }
    
    func setAutoTimer(){
        self.autoTimer.invalidate()
        self.autoTimer = Timer.scheduledTimer(timeInterval: AUTO_REFRESH_TIME_30, target: self, selector: #selector(HomeViewController.getVehicleListDataFromServer), userInfo: nil, repeats: true)
    }
    
    //MARK: - Get Data From Server
    @objc func getVehicleListDataFromServer() {
        if (self.fleetListRequest != nil) {
            return
        }
        if(fleetListArray == nil){
            self.showProgress()
        }else{
            if(fleetListArray.count == 0){
                self.showProgress()
            }
        }
        var userIdParam:NSString = ((appDelegate.loggedUser?.value(forKey: KEY_RESPONSE_USER_ID)! as AnyObject).stringValue! as NSString)
        if let clientID:NSString = appDelegate.loggedUser?.value(forKey: KEY_RESPONSE_CLIENT_USER_ID) as? NSString {
            userIdParam = (userIdParam as String) + ":" + (clientID as String) as NSString//userIdParam.stringByAppendingFormat(":%@", clientID)
        }
        let paramDict:NSDictionary = [KEY_REQUEST_USER_ID:userIdParam];
//        self.refreshControl.beginRefreshing()
        
        self.fleetListRequest = WebApiController.sharedInstance.getFleetList(paramDict: paramDict, onSuccess: { (response:AnyObject?) -> Void in
//            self.refreshControl.endRefreshing()
            self.fleetListRequest = nil;
            self.hideProgress()
            self.processResponse(response: response!)
        }) { (error:NSError?) -> Void in
//            self.refreshControl.endRefreshing()
            self.fleetListRequest = nil;
            //                self.hideProgress()
            if(error?.code == ERROR_CODE_USER_CANCELLED) {
                return
            }
            self.showAlertError(error: error!)
        }
    }
    
    //MARK:- Response
    func processResponse(response:AnyObject) {
        queue.async() {
            // do some task
            
            self.tableArray.removeAll()
            
            self.responseDict = response as? NSDictionary
//            self.appDelegate.fleetListResponse = response as? NSDictionary
            
            if(self.groupParamDict != nil) {
//                self.processForSelectedGroup()
            } else {
                self.fleetListArray = (self.responseDict!.value(forKey: KEY_RESPONSE_FLEET_LIST) as? NSArray)!
                
                if self.isSearching == false {
//                    self.groupList = self.responseDict!.value(forKeyPath: KEY_RESPONSE_FLEET_GROUP_LIST) as? NSArray
//                    let descriptor: NSSortDescriptor = NSSortDescriptor(key: "event_ts", ascending: true)
//                    let sortedResults: NSArray = self.groupList!.sortedArray(using: [descriptor]) as NSArray
//
//                    self.tableArray!.addObjects(from: sortedResults as! [NSDictionary])
                    // do nothing
                } else {
                    let bPredicate: NSPredicate = NSPredicate(format: "SELF.registration contains[cd] %@", self.searchBar.text!)
                    self.fleetListArray = self.fleetListArray.filtered(using: bPredicate) as NSArray
                    
                    
                }
                for vehicle in self.fleetListArray{
                    let mVehicle = FleetMap.init(param: vehicle as! NSDictionary)
                    self.tableArray.append(mVehicle)
                }
// var ready = convertedArray.sorted(by: { $0.compare($1) == .orderedDescending })
                self.tableArray = self.tableArray.sorted(by: { $0.fullDate?.compare($1.fullDate!) == .orderedDescending })

            }
            
//            dispatch_get_main_queue().sync()
            DispatchQueue.main.sync(){
                // update some UI
                self.tableView.estimatedRowHeight = 100.0
                self.tableView.rowHeight = UITableViewAutomaticDimension
                self.tableView.reloadData()
            }
        }
    }
    
    //MARK:- Table View Delegate and DATA Source
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.tableArray.count
    }
    
    func  numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    
     func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if(self.searchBar.text!.length > 0){
            cnlblSearchResultHeight.constant = 39
            return 90
        }else{
            cnlblSearchResultHeight.constant = 0
            return 65
        }
//        return 50
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        /*
        let viewHeader : UIView =   UIView.init(frame: CGRect.init(x: 0, y: -2, width: tableView.frame.width, height: 200))
        viewHeader.backgroundColor = .clear//UIColor.init(hex: "303C49")
        
        self.searchBar = UISearchBar.init(frame: CGRect.init(x: 0, y: 0, width: tableView.frame.width, height: 50))
        self.searchBar.backgroundColor = UIColor.init(hex: "303C49")
        self.searchBar.tintColor = UIColor.init(hex: "303C49")
        self.searchBar.barTintColor = UIColor.init(hex: "303C49")
        self.searchBar.setImage(UIImage.init(named: "search"), for: .search, state: .normal)
        self.searchBar.delegate = self
        self.searchBar.inputAccessoryView = self.addToolBar()
        let viewHeader1 : UIView =   UIView.init(frame: CGRect.init(x: 0, y: -1, width: tableView.frame.width, height: 2))
        viewHeader1.backgroundColor = UIColor.init(hex: "303C49", alpha: 1)
        
        viewHeader.addSubview(self.searchBar)
        viewHeader.addSubview(viewHeader1)
//        viewHeader.addSubview(viewSearchResult)
        return viewHeader
 */
        let viewHeader1 : UIView =   UIView.init(frame: CGRect.init(x: 0, y: -1, width: tableView.frame.width, height: 2))
        viewHeader1.backgroundColor = UIColor.init(hex: "303C49", alpha: 1)
        viewSearchResult.addSubview(viewHeader1)
        self.searchBar.backgroundColor = UIColor.init(hex: "303C49")
        self.searchBar.tintColor = UIColor.init(hex: "303C49")
        self.searchBar.barTintColor = UIColor.init(hex: "303C49")
        self.searchBar.setImage(UIImage.init(named: "search"), for: .search, state: .normal)
        self.searchBar.inputAccessoryView = self.addToolBar()
        return viewSearchResult
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellIndentifier:String = "fleetList"
        let iscellGroup:Bool = false
        
        
//        if(isSearching == true || indexPath.row >= self.groupList?.count) {
//            cellIndentifier =  "fleetList"
//            iscellGroup = false
//        }
        
        let cell:FleetListTableViewCell = tableView.dequeueReusableCell(withIdentifier: cellIndentifier) as! FleetListTableViewCell
//        if indexPath.row <= ((self.tableArray?.count)! - 1) {
        cell.setUpFleetCell(cellData: self.tableArray[indexPath.row] as AnyObject, isGroup:iscellGroup)
//        }
        
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("SELECTED INDEX :: \(indexPath.row)")
        let vehicle:FleetMap = self.tableArray[indexPath.row]
        self.performSegue(withIdentifier: "sgShowTripList", sender: vehicle)
    }

    // MARK: - search bar delegate
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String){
        self.tableArray.removeAll()
        let arrfilter:NSArray!
        if(searchText.length > 0){
            self.isSearching = true
            let bPredicate: NSPredicate = NSPredicate(format: "SELF.registration contains[cd] %@", searchText)
            arrfilter = self.fleetListArray.filtered(using: bPredicate) as NSArray
            lblSearchResult.text = [TR_LBL_FOUND," ",String(arrfilter.count)," ",TR_LBL_VEHICEL_MATCHING].joined()
            cnlblSearchResultHeight.constant = 39
        }else{
            arrfilter = self.fleetListArray
            lblSearchResult.text = ""
            cnlblSearchResultHeight.constant = 0
            self.isSearching = false
        }
        for vehicle in arrfilter{
            let mVehicle = FleetMap.init(param: vehicle as! NSDictionary)
            self.tableArray.append(mVehicle)
        }
        self.tableArray = self.tableArray.sorted(by: { $0.fullDate?.compare($1.fullDate!) == .orderedDescending })
        self.tableView.reloadData()
    }
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "sgShowTripList"{
            if let tripList:TripListViewController = segue.destination as? TripListViewController {
                let vehicle:FleetMap = sender as! FleetMap
                tripList.vehicle = vehicle
            }
        } 
    }
}

