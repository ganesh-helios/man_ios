//
//  FleetListTableViewCell.swift
//  ManApp
//
//  Created by Ganesh on 01/11/17.
//  Copyright © 2017 Helios Solutions. All rights reserved.
//

import UIKit

class FleetListTableViewCell: UITableViewCell {

    @IBOutlet weak var imgIgnition: UIImageView!
    @IBOutlet weak var lblRegistration: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblAddress: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setUpFleetCell(cellData:AnyObject, isGroup:Bool) {
        let vehicle:FleetMap = cellData as! FleetMap
        if(vehicle.ignition! <= 1){
            imgIgnition.image = #imageLiteral(resourceName: "key_gray")
        }else{
            imgIgnition.image = #imageLiteral(resourceName: "key")
        }
        self.lblRegistration.text = vehicle.registration
        self.lblAddress.text = vehicle.location!
        self.lblDate.text = Utility.changeFormateOfStringDate(strDate: vehicle.time!, strRequireFormate: DATE_FORMAT_TIME, strCurrentFormate: DATE_FORMAT_API)
    }
}
