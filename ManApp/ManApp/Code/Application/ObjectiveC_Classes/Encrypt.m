//
//  Encrypt.m
//  Fleet
//
//  Created by Nicolas Pearson on 2015/04/23.
//  Copyright (c) 2015 Cartrack. All rights reserved.
//

#import "Encrypt.h"

@implementation Encrypt

#define HexCharToNybble(x) ((char)((x > '9') ? tolower(x) - 'a' + 10 : x - '0') & 0xF)

static const char _base64EncodingTable[64] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";

- (NSString*) sha1:(NSString*)input
{
    const char *cstr = [input cStringUsingEncoding:NSUTF8StringEncoding];
    NSData *data = [NSData dataWithBytes:cstr length:input.length];
    
    uint8_t digest[CC_SHA1_DIGEST_LENGTH];
    
    CC_SHA1(data.bytes, (int) data.length, digest);
    
    NSMutableString* output = [NSMutableString stringWithCapacity:CC_SHA1_DIGEST_LENGTH * 2];
    
    for(int i = 0; i < CC_SHA1_DIGEST_LENGTH; i++)
        [output appendFormat:@"%02x", digest[i]];
    
    return output;
    
}

- (NSString *) hashToBase64:(NSString *) input
{
    NSString *hexInput = [self sha1:input];
    
    NSUInteger length = [hexInput length] / 2;
    
    NSMutableData * data = [NSMutableData alloc];
    
    char byte_chars[2] = {'\0','\0'};
    char byteToAppend = '0';
    for (int i = 0; i < length; i++)
    {
        byte_chars[0] = [hexInput characterAtIndex:i*2];
        byte_chars[1] = [hexInput characterAtIndex:i*2+1];
        
        byteToAppend = (HexCharToNybble(byte_chars[0]) << 4) + HexCharToNybble(byte_chars[1]);
        
        [data appendBytes:&byteToAppend length:1];
    }
    
    return [self encodeBase64WithData:data];
}

- (NSString *) encodeBase64WithString:(NSString *) strData
{
    return [self encodeBase64WithData:[strData dataUsingEncoding:NSUTF8StringEncoding]];
}

- (NSString *) encodeBase64WithData:(NSData *) objData
{
    const unsigned char * objRawData = [objData bytes];
    char * objPointer;
    char * strResult;
    
    // Get the Raw Data length and ensure we actually have data
    NSUInteger intLength = [objData length];
    if (intLength == 0) return nil;
    
    // Setup the String-based Result placeholder and pointer within that placeholder
    strResult = (char *)calloc(((intLength + 2) / 3) * 4, sizeof(char));
    objPointer = strResult;
    
    // Iterate through everything
    // keep going until we have less than 24 bits
    while (intLength > 2)
    {
        *objPointer++ = _base64EncodingTable[objRawData[0] >> 2];
        *objPointer++ = _base64EncodingTable[((objRawData[0] & 0x03) << 4) + (objRawData[1] >> 4)];
        *objPointer++ = _base64EncodingTable[((objRawData[1] & 0x0f) << 2) + (objRawData[2] >> 6)];
        *objPointer++ = _base64EncodingTable[objRawData[2] & 0x3f];
        
        // we just handled 3 octets (24 bits) of data
        objRawData += 3;
        intLength -= 3;
    }
    
    // now deal with the tail end of things
    if (intLength != 0)
    {
        *objPointer++ = _base64EncodingTable[objRawData[0] >> 2];
        
        if (intLength > 1)
        {
            *objPointer++ = _base64EncodingTable[((objRawData[0] & 0x03) << 4) + (objRawData[1] >> 4)];
            *objPointer++ = _base64EncodingTable[(objRawData[1] & 0x0f) << 2];
            *objPointer++ = '=';
        }
        else
        {
            *objPointer++ = _base64EncodingTable[(objRawData[0] & 0x03) << 4];
            *objPointer++ = '=';
            *objPointer++ = '=';
        }
    }
    
    // Terminate the string-based result
    *objPointer = '\0';
    
    NSString * result = [NSString stringWithCString:strResult encoding:NSASCIIStringEncoding];
    free(strResult);
    
    // Return the results as an NSString object
    return result;
}

@end

