//
//  Encrypt.h
//  Fleet
//
//  Created by Nicolas Pearson on 2015/04/23.
//  Copyright (c) 2015 Cartrack. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CommonCrypto/CommonDigest.h>

@interface Encrypt : NSObject
{
    
}

-(NSString *) sha1:(NSString *)input;
-(NSString *) encodeBase64WithData:(NSData *) objData;
-(NSString *) encodeBase64WithString:(NSString *) strData;
-(NSString *) hashToBase64:(NSString *) input;

@end