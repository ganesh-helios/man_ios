//
//  Utility.swift
//  Communicator
//
//  Created by Diptesh Patel on 01/03/16.
//  Copyright © 2016 KinderKastle. All rights reserved.
//

import Foundation
import UIKit


class Utility{
    
//    func condenseWhitespace(string: String) -> String {
//        let components = string.componentsSeparatedByCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet()).filter({!isEmpty($0)})
//        return join(" ", components)
//    }

    static let sharedInstance  = Utility()
    var documentsPath:URL!
    var imagesPath:URL!
    let fileManager = FileManager.default
    
    //MARK: Set Value to user default
    
    //MARK: Set Value to user default
    class func userDefaultsSetValue(value: AnyObject?,key: String)
    {
        
        let defaults = UserDefaults.standard
        defaults.set(value , forKey: key as String)
        defaults.synchronize()
    }
    
    //MARK: - userDefaultsGetValue
    class func userDefaultsGetValue(key:String) -> AnyObject? {
        let defaults = UserDefaults.standard
        return defaults.value(forKey: key) as AnyObject?
    }
    //MARK: Get string value from user default
    class func userDefaultsGetStringValue(key: String) ->String
    {
        
        let defaults = UserDefaults.standard
        let returnValue = defaults.string(forKey: key as String)
        if (returnValue != nil) {
            return returnValue!
        } else {
            return ""
        }
    }
    
    class func heightForLabel(_ text:String, font:UIFont, width:CGFloat) -> CGFloat
    {
        let constraintRect = CGSize(width: width, height: CGFloat.greatestFiniteMagnitude)
        let boundingBox = text.boundingRect(with: constraintRect, options: NSStringDrawingOptions.usesLineFragmentOrigin, attributes: [NSAttributedStringKey.font: font], context: nil)
        
        return boundingBox.height+1
    }

    class func stringfromDate(_ date:Date,withTimeZone timezone:TimeZone? = TimeZone.autoupdatingCurrent) ->String {
        let formatter:DateFormatter = DateFormatter.init()
        formatter.dateFormat = DATE_FORMAT_DATE
        formatter.timeZone = timezone
        formatter.locale =  Locale(identifier: "en_US_POSIX")
        return formatter.string(from: date)
    }
    class func change24TimeFromTimeString(_ strTime:String) -> String{
        let dateAsString = strTime
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "hh:mm a"
        let date = dateFormatter.date(from: dateAsString)
        
        dateFormatter.dateFormat = "HH:mm"
        let date24 = dateFormatter.string(from: date!)
        return date24
    }
    
    class func change12TimeFromTimeString(_ strTime:String) -> String{
        let dateAsString = strTime
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "HH:mm:ss"
        let date = dateFormatter.date(from: dateAsString)
        
        dateFormatter.dateFormat = "hh:mm a"
        let date12 = dateFormatter.string(from: date!)
        return date12
    }
    
    class func stringfromDateFormat(_ date:Date, stringDateFormat:String) ->String
    {
        let formatter:DateFormatter = DateFormatter.init()
        formatter.dateFormat = stringDateFormat
        formatter.timeZone = TimeZone.autoupdatingCurrent
        formatter.locale =  Locale(identifier: "en_US_POSIX")
        return formatter.string(from: date)
    }

    
    class func dateFromStringDate(_ strDate:NSString,stringDateFormate:String) -> Date
    {
        let formatter:DateFormatter = DateFormatter.init()
        formatter.dateFormat = stringDateFormate
        formatter.locale =  Locale(identifier: "en_US_POSIX")
        formatter.timeZone = TimeZone.init(secondsFromGMT: 0)
        return formatter.date(from: strDate as String)!
    }
    
    class func dateFromStringDateWithDefaultTimeZone(_ strDate:NSString,stringDateFormate:String) -> Date
    {
        let formatter:DateFormatter = DateFormatter.init()
        formatter.dateFormat = stringDateFormate
        formatter.locale =  Locale(identifier: "en_US_POSIX")
        formatter.timeZone = TimeZone.autoupdatingCurrent
        return formatter.date(from: strDate as String)!
    }
    
    class func dateFromDOBString(_ strDate:String) -> Date {
        let formatter:DateFormatter = DateFormatter()
        formatter.dateFormat = DATE_FORMAT_DOB
        return formatter.date(from: strDate)!
    }
    
    class func dateFromStringAPIDate(_ strDate:String) -> Date
    {
        let formatter:DateFormatter = DateFormatter.init()
        formatter.dateFormat = DATE_FORMAT_API
        formatter.locale =  Locale(identifier: "en_US_POSIX")
//        formatter.locale =  Locale(identifier:Utility.userDefaultsGetStringValue(CURRENT_LANGUAGE_DATE_LANGUAGE as String))
//        formatter.timeZone = TimeZone.init(secondsFromGMT: 0)
        return formatter.date(from: strDate as String)!
    }
    
    class func localTimeOnlyFromString(_ dateStr:String) -> String {
        if (dateStr.length == 0) {
            return dateStr;
        }
        
        let formatter:DateFormatter = DateFormatter.init()
        formatter.dateFormat = DATE_FORMAT_API
        formatter.timeZone = TimeZone.autoupdatingCurrent
        formatter.locale =  Locale(identifier: "en_US_POSIX")
        let date:Date = formatter.date(from: dateStr as String)!
        
        formatter.dateFormat = DATE_FORMAT_TIME
        return formatter.string(from: date)
        
    }
    class func localTimeFromString(_ dateStr:String) ->String
    {
        if (dateStr.length == 0) {
            return dateStr;
        }
        
        let formatter:DateFormatter = DateFormatter.init()
        formatter.dateFormat = DATE_FORMAT_API
        formatter.timeZone = TimeZone.autoupdatingCurrent
        formatter.locale =  Locale(identifier: "en_US_POSIX")
        let date:Date = formatter.date(from: dateStr as String)!
        
        formatter.dateFormat = DATE_FORMAT_VIEW
        return formatter.string(from: date)
    }
    
    
    class func formattedNumber(_ number:Double) -> String {
        let formatter:NumberFormatter = NumberFormatter.init()
        formatter.numberStyle = .currency
        formatter.currencySymbol = ""
        formatter.maximumFractionDigits = 0
        formatter.roundingMode = .floor
        
        return formatter.string(from: NSNumber.init(value: number as Double))!
    }
    
    class func clearAllUserDefaults()
    {
        
        UserDefaults.standard.removePersistentDomain(forName: Bundle.main.bundleIdentifier!)
        UserDefaults.standard.synchronize()

    }
    
    class func getDateInMonthYearFormat(_ dateStr: String) -> String{
        let timeStr: String = dateStr
        let dateFormatter1 = DateFormatter.init()
        dateFormatter1.dateFormat = "MM yyyy"//"yyyy-MM-dd HH:mm:ss Z"
        
        let myTime:Date = dateFormatter1.date(from: timeStr)!
        // Get Only Time
        dateFormatter1.locale = Locale(identifier: "en_US") as Locale
        dateFormatter1.dateFormat = "MMMM yyyy"
        let generatedString = dateFormatter1.string(from: myTime)
        return generatedString
    }
    
    class func getCurrentDate() -> String{
        let formatter:DateFormatter = DateFormatter.init()
//        formatter.dateFormat = DATE_FORMAT_API
        let dateStart:Date = Date()
        formatter.timeZone = TimeZone.autoupdatingCurrent
        formatter.locale =  Locale(identifier: "en_US_POSIX")
        formatter.dateFormat = DATE_FORMAT_DATE
        return formatter.string(from: dateStart)
    }
    
    class func changeFormateOfStringDate(_ strDate:String,strRequireFormate:String,strCurrentFormate:String) ->String
    {
        let date:Date = self.dateFromStringDateWithFormat(strDate as NSString,strFormate: strCurrentFormate)
        let formatter:DateFormatter = DateFormatter.init()
        formatter.dateFormat = strRequireFormate
        formatter.timeZone = TimeZone.autoupdatingCurrent
        formatter.locale =  Locale(identifier: "en_US_POSIX")
    
        return formatter.string(from: date)
    }
    
    class func dateFromStringDateWithFormat(_ strDate:NSString,strFormate:String) -> Date
    {
        let formatter:DateFormatter = DateFormatter.init()
        formatter.dateFormat = strFormate
        formatter.locale =  Locale(identifier: "en_US_POSIX")
        formatter.timeZone = TimeZone.init(secondsFromGMT: 0)
        //formatter.timeZone = NSTimeZone.local
        return formatter.date(from: strDate as String)!
    }
    
    //MARK: - changeFormateOfStringDate
    class func changeFormateOfStringDate(strDate:String,strRequireFormate:String,strCurrentFormate:String) ->String{
        let date:NSDate = Utility.dateFromStringDateWithFormat(strDate as NSString,strFormate: strCurrentFormate) as NSDate
        let formatter:DateFormatter = DateFormatter.init()
        formatter.dateFormat = strRequireFormate
        formatter.timeZone = NSTimeZone.local
        formatter.locale =  NSLocale(localeIdentifier: "en_US_POSIX") as Locale!
        return formatter.string(from: date as Date)
    }
    
    static func degreesToRadians(_ degrees: Double) -> Double { return degrees * M_PI / 180.0 }
    
    class func getAgeFromBirthDate(_ birthDate:Date) -> Int{
        let calendar = Calendar.current
        let ageComponents = (calendar as NSCalendar).components(.month,
                                                from: birthDate,
                                                to: Date(),
                                                options: [])
        var age = ageComponents.month // return age in month
        if(age == 0){
            age = 1
        }
        return age!
    }

    class func getYearFromDate(_ date:Date) -> Int{
        let calendar = Calendar.current
        let ageComponents = (calendar as NSCalendar).components(.year,
                                                from: date,
                                                to: Date(),
                                                options: [])
        let age = ageComponents.year // return age in yaer
        return age!
    }
    
    //MARK:- Get ALL Week dates
    class func getDateByAddingDays(_ startDate : Date , noOfDays: Int) -> Date {
        let calendar = Calendar.current
        let sixDays = (calendar as NSCalendar).date(byAdding: .day, value: noOfDays, to: startDate, options: [])
//        print("START : \(startDate) *** END DATE::  \(sixDays)")
        return sixDays!
    }
    
    
    class func formattedDaysInThisWeek() -> [String] {
        // create calendar
        let calendar = Calendar(identifier: Calendar.Identifier.gregorian)
        // today's date
        let today = Date()
        let todayComponent = (calendar as NSCalendar).components([.day, .month, .year], from: today)
        
        // range of dates in this week
        let thisWeekDateRange = (calendar as NSCalendar).range(of: .day, in:.weekOfMonth, for:today)
        
        // date interval from today to beginning of week
        let dayInterval = thisWeekDateRange.location - todayComponent.day!
        
        // date for beginning day of this week, ie. this week's Sunday's date
        let beginningOfWeek = (calendar as NSCalendar).date(byAdding: .day, value: dayInterval, to: today, options: .matchNextTime)
        
        var formattedDays: [String] = []
        
        for i in 0 ..< thisWeekDateRange.length {
            let dateIs:Date = (calendar as NSCalendar).date(byAdding: .day, value: i, to: beginningOfWeek!, options: .matchNextTime)!
            formattedDays.append(formatDate(dateIs))
        }
        
        return formattedDays
    }

    //MARK:- Format date in format
    class func formatDate(_ date: Date) -> String {
        let format = "EEE,MM/dd"
        let formatter = DateFormatter()
        formatter.dateFormat = format
        formatter.locale =  Locale(identifier: "en_US_POSIX")
        formatter.timeZone = TimeZone.init(secondsFromGMT: 0)
        
        return formatter.string(from: date)
    }

    
    class func formatDateWeek(_ date: Date) -> String {
        let format = "MMM dd"
        let formatter = DateFormatter()
        formatter.dateFormat = format
        formatter.locale =  Locale(identifier: "en_US_POSIX")
        formatter.timeZone = TimeZone.init(secondsFromGMT: 0)
        
        return formatter.string(from: date)
    }

    
    //MARK:- Format date in format
    class func getZeroTimeDate(_ date: Date) -> Date {
        let format = "yyyy-MM-dd HH:mm:ss"
        let formatter = DateFormatter()
        formatter.dateFormat = format
        formatter.locale =  Locale(identifier: "en_US_POSIX")
        formatter.timeZone = TimeZone.init(secondsFromGMT: 0)
        
        let strDate = formatter.string(from: date)
        let zeroTimeDate : Date = formatter.date(from: strDate)!
        return zeroTimeDate
    }

    class func formatNewDateForDate(_ date: Date) -> String {
        let format = "yyyy-MM-dd"
        let formatter = DateFormatter()
        formatter.dateFormat = format
        formatter.locale =  Locale(identifier: "en_US_POSIX")
        formatter.timeZone = TimeZone.init(secondsFromGMT: 0)
        
        return formatter.string(from: date)
    }
    
	
	//MARK:- Get START AND END the dates in 3 month month
	class func getStartEndDateOfMonth() ->[Date] {
		
		var dateArray : [Date] = [Date]()
		
		let calendar = Calendar(identifier: Calendar.Identifier.gregorian)
		let today = Date()
		let components = (calendar as NSCalendar).components([.year, .month], from: today)
		let startOfMonth = calendar.date(from: components)!
		
		var comps2 = DateComponents()
		comps2.month = 1
		comps2.day = -1
		let endOfMonth = (calendar as NSCalendar).date(byAdding: comps2, to: startOfMonth, options: [])!
		
//		///Next month Dates
//		comps2.month = 1
//		comps2.day = 1
//		(comps2 as NSDateComponents).date
//		
//		let startOfMonthDate = (calendar as NSCalendar).date(byAdding: comps2, to: endOfMonth, options: [])!
//		
		
		dateArray.append(startOfMonth)
		dateArray.append(endOfMonth)
		
		return dateArray
	}

	
    //MARK:- Get START AND END the dates in 3 month month
    class func getAllDatesFromMonth() ->[Date] {
		
        var dateArray : [Date] = [Date]()
		
        let calendar = Calendar(identifier: Calendar.Identifier.gregorian)
        let today = Date()
        let components = (calendar as NSCalendar).components([.year, .month], from: today)
        let startOfMonth = calendar.date(from: components)!
        
        var comps2 = DateComponents()
        comps2.month = 1
        comps2.day = -1
        let endOfMonth = (calendar as NSCalendar).date(byAdding: comps2, to: startOfMonth, options: [])!
        
        ///Next month Dates
        comps2.month = 2
        comps2.day = 1
        (comps2 as NSDateComponents).date
        
        let nextMonthEndDate = (calendar as NSCalendar).date(byAdding: comps2, to: endOfMonth, options: [])!
        
        ///Prev month Dates
        comps2.month = -2
        comps2.day = 1
        (comps2 as NSDateComponents).date
        let prevMonthStartDate = (calendar as NSCalendar).date(byAdding: comps2, to: startOfMonth, options: [])!

        
        dateArray.append(prevMonthStartDate)
        dateArray.append(nextMonthEndDate)
        
        return dateArray
    }
    //MARK:- Get
    
    class func generateDates(_ startDate: Date, endDate: Date) -> [Date] {
        
        var myDateArray : [Date] = [Date]()
        let calendar = Calendar.current
        var startDate = startDate
        let endDate = endDate
        
        var isFirstMonday :Int = 0

        while startDate.compare(endDate) != .orderedDescending {
            let dateString = formatDate(startDate)
            let removeSunSaturday : Bool = dateString.contains("Sat") ? true : (dateString.contains("Sun") ? true : false)
            
            if removeSunSaturday == false
            {
                if isFirstMonday == 0
                {
                    let isMonday : Bool = dateString.contains("Mon") ? true : false
                    if (isMonday == true) {
                        isFirstMonday = 1
                    }
                }
                
                if isFirstMonday == 1 {
//                    print("DATES :: \(dateString) ** :: \(startDate) *** \(NSDate())")
                    myDateArray.append(startDate)
                }
            }
            // Advance by one day:
            startDate = (calendar as NSCalendar).date(byAdding: .day, value: 1, to: startDate, options: [])!
        }
        
        var isLastFriday : Int = 0
        var indexIs : Int = 0
        for datesAre in myDateArray.reversed() {
            let dateString = formatDate(datesAre)
            if isLastFriday == 0
            {
                let isFriday : Bool = dateString.contains("Fri") ? true : false
                if (isFriday == false) {
                    myDateArray.removeLast()
                    isLastFriday = 1
                    break
                }
            }
            indexIs += 1

        }
        
        return myDateArray
    }
    
    class func currentTimeMillis() -> Int64{
        let nowDouble = Date().timeIntervalSince1970
        return Int64(nowDouble*1000)
    }
    
    class func createDocumentsFolder(_ folderName:String){
       Utility.sharedInstance.documentsPath = URL(fileURLWithPath: NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0])
        print(Utility.sharedInstance.documentsPath)
        Utility.sharedInstance.imagesPath = Utility.sharedInstance.documentsPath.appendingPathComponent(folderName)
        if (!Utility.sharedInstance.fileManager.fileExists(atPath: Utility.sharedInstance.imagesPath.path)){
            do {
                try FileManager.default.createDirectory(atPath: Utility.sharedInstance.imagesPath.path, withIntermediateDirectories: true, attributes: nil)
            } catch let error as NSError {
                NSLog("Unable to create directory \(error.debugDescription)")
            }
        }
    }
    
    
    class func phoneNumberFormat(nsString:NSString,range:NSRange, strText:String?) -> String{
        let newString = nsString.replacingCharacters(in: range, with: strText!)
        let components = newString.components(separatedBy: CharacterSet.decimalDigits.inverted)
        //            let emptryString:NSString = ""
        let decimalString = NSString.init(string: (components.joined(separator: "")))
        
        let length = decimalString.length
        let hasLeadingOne = length > 0 && decimalString.character(at: 0) == (1 as unichar)
        
        var index = 0 as Int
        let formattedString = NSMutableString()
        
        if hasLeadingOne
        {
            formattedString.append("1 ")
            index += 1
        }
        if (length - index) > 3
        {
            let areaCode = decimalString.substring(with: NSMakeRange(index, 3))
            formattedString.appendFormat("%@-", areaCode)
            index += 3
        }
        if length - index > 3
        {
            let prefix = decimalString.substring(with: NSMakeRange(index, 3))
            formattedString.appendFormat("%@-", prefix)
            index += 3
        }
        
        let remainder = decimalString.substring(from: index)
        formattedString.append(remainder)
        return formattedString as String
    }
    
    //MARK: - Check Internet
    class func checkInternet() -> Bool{
        return (WebApiController.sharedInstance.reachabilityManager?.isReachable)!
    }
    
    
}
