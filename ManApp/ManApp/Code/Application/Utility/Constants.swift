//
//  Constants.swift
//  Communicator
//
//  Created by Diptesh Patel on 01/03/16.
//  Copyright © 2016 KinderKastle. All rights reserved.
//

import Foundation
import UIKit


var URLBASE = "https://"
var URLDB = ""
var URLLOGIN = "/service/login.php"
var URLVEHICLESLIST = "/service/svc2.php/clients/%s/vehicles"
var URLVEHICLETRIPS = "/service/svc2.php/clients/%s/vehicles/%i/trips/datelist/sdate/%f/edate/%f"

let MAP_TILE_URL = "mapalt.php/%d/%d/%d.png"
let MAP_TILE_URL_STRING = "mapalt.php/%@/%@/%@.png"

let MAP_TILE_URL_NL = "mapnik.php/%d/%d/%d.png"
let MAP_TILE_URL_STRING_NL = "mapnik.php/%@/%@/%@.png"



var DATE_FORMAT_API     = "yyyy-MM-dd HH:mm:ssZ"
let APP_DELEGATE        = UIApplication.shared.delegate as! AppDelegate

//var DATE_FORMAT_DATE            = "MM-dd-yyyy"
var DATE_FORMAT_DATE            = "yyyy-MM-dd"
var DATE_FORMAT_DATE_SLASH      = "MM/dd/yy"
var DATE_FORMAT_DATE_YEAR       = "yyyy-MM-dd"
var DATE_FORMAT_TIME            = "hh:mm a"
var DATE_FORMAT_DAY_TIME        = "EEE - hh:mm a"
var DATE_FORMAT_24_HOUR_TIME    = "HH:mm"
var DATE_FORMAT_DATE_WITH_TIME  = "MM-dd-yyyy hh:mm a"

var DATE_FORMAT_VIEW         = "yyyy-MM-dd HH:mm:ss"
var DATE_FORMATE_MONTH       = "M"
var DATE_FORMATE_YEAR       = "yyyy"

var DATE_FORMAT_ENCRYPT      = "yyyy-MM-dd'T'HH:mm:ss'Z'"
var DATE_FORMAT_DOB          = "yyyy-MM-dd'T'HH:mm:ss"
var DATE_FORMAT_DATE_TIME    = "yyyy-MM-dd hh:mm a'Z'"
var DATE_FORMAT_TIME_ONLY    = "HH:mm:ss"
var DATE_FORMAT_MSG          = "MMM dd"
var DATE_FORMAT_EMAIL        = "dd MMM, HH:mm a"
var DATE_FORMAT_TIME_WITH_ZONE = "HH:mm:ssZ"

//Fonts
func FONT_MYRIAD(_ size:CGFloat)->UIFont{ return UIFont.init(name: "MyriadPro-Regular", size: size)!}
func FONT_MYRIAD_BOLD(_ size:CGFloat)->UIFont{ return UIFont.init(name: "MyriadPro-Bold", size: size)!}
func FONT_ROBOTO(_ size:CGFloat)->UIFont{ return UIFont.init(name: "Roboto-Regular", size: size)!}
func FONT_ROBOTO_BOLD(_ size:CGFloat)->UIFont{ return UIFont.init(name: "Roboto-Bold", size: size)!}

let KEY_DEFAULTS_LAST_SYNC_TIME     = "LAST_SYNC_TIME" as NSString
let KEY_DEFAULTS_DELETED_DEVICES    = "DELETED_DEVICES_LIST" as String

let KEY_DEFAULT_LAST_LOGIN          = "KEY_LAST_LOGIN" as String
let KEY_DEFAULT_LAST_LOGIN_DATA     = "KEY_LAST_LOGIN_DATA" as String

let KEY_DEFAULT_LAST_HOSPITAL_LIST  = "LAST_HOSPITAL_LIST" as String

let KEY_DEFAULT_NOTIFICATION_ID     = "NOTIFICATION_ID" as String

let KEY_CENTER_PROFILE_ADDRESS      = "CENTER_PROFILE_ADDRESS" as String
let KEY_CENTER_SCHOOL_ID            = "CENTER_SCHOOL_ID" as String

//Notifications
let SHOW_STICKY_NOTES  = NSNotification.Name(rawValue: "ShowAssistiveTouch")
let HIDE_STICKY_NOTES  = NSNotification.Name(rawValue: "HideAssistiveTouch")
let UPDATE_ADD_DAILY_NOTE    = NSNotification.Name(rawValue: "UpdateAndAddDailyNote")
// Depending on Country Selected by user this could be km/h or miles/h
var USE_METRIC_UNIT_KM_PER_HOUR = "km/h"
var USE_METRIC_UNIT_KM = "km"
var USE_METRIC_UNIT = true


let PLAY_TRIP_INTERVAL                          = 1.0
let DEFAULT_CONNECTION_TIMEOUT:TimeInterval   = 25
let AUTO_REFRESH_TIME:TimeInterval            = 60
let AUTO_REFRESH_TIME_30:TimeInterval            = 30

//#define CURRENT_BUNDLE              [NSBundle bundleWithPath:[[NSBundle mainBundle] pathForResource:[[NSUserDefaults standardUserDefaults] valueForKey:KEY_LANGUAGE_CODE] ofType:@"lproj"]]

//let CURRENT_BUNDLE:NSBundle = NSBundle.mainBundle().pathForResource("en", ofType: "lproj")
//#define		TR_CREATE_TABLE							NSLocalizedStringFromTableInBundle(@"create_table",@"Transalations", CURRENT_BUNDLE, @"")
//let TR_CREATE_TABLE = NSLocalizedString("key", tableName: "table", bundle: CURRENT_BUNDLE, value: "", comment: "")


//let FONT_ROBOTO_REGULAR      = "Roboto-Regular"
//let FONT_ROBOTO_BOLD         = "Roboto-Bold"
//let IPAD_FONT_SIZE20 = CGFloat(20.0)
//let IPAD_FONT_SIZE19 = CGFloat(19.0)
//let IPAD_FONT_SIZE18 = CGFloat(18.0)
//let IPAD_FONT_SIZE17 = CGFloat(17.0)
//let IPAD_FONT_SIZE16 = CGFloat(16.0)
//let IPAD_FONT_SIZE15 = CGFloat(15.0)
//let IPAD_FONT_SIZE14 = CGFloat(14.0)
//let IPAD_FONT_SIZE13 = CGFloat(13.0)

let METER_TO_MILE   = 0.000621371
let KM_TO_MILE  = 0.621371

//Colors
let COLOR_FFFFFF  = "#ffffff"

let KEY_USER_ACCOUNT                    = "user_account"
let KEY_USER_SUB_ACCOUNT                = "user_subaccount"
let KEY_USER_PASSWORD                   = "user_password"
let KEY_USER_SELECTED_COUNTRY           = "user_selected_country"
let KEY_SERVER_URL                      = "fullURL"
let KEY_COUNTRY_URL                     = "CountryURL"
let KEY_COOKIE                          = "Cookie"
var CURRENT_LANGUAGE_DATE_LANGUAGE      = "current_language_date_formate"

// REQUEST
// Login
let KEY_REQUEST_USER_NAME               = "userName"
let KEY_REQUEST_CLIENT_USER_NAME        = "clientUserName"
let KEY_RESPONSE_CLIENT_USER_NAME       = "client_user_name"
let KEY_REQUEST_NOUNCE                  = "nonce"
let KEY_REQUEST_CREATED                 = "created"
let KEY_REQUEST_PASSWORD                = "password"
let KEY_REQUEST_FORCE_ROAD_SPEED        = "forceRoadSpeed"
let KEY_REQUEST_CLIENT_SRC_ID           = "client_src_id"
let KEY_REQUEST_LOCALE                  = "locale"
let KEY_REQUEST_USER_ID                 = "user_id"
let KEY_USE_MATRIC                      = "useMetric"

// FLEET MAP
let KEY_REQUEST_VEHICLE_ID              = "vehicle_id"

// TRIP LIST
let KEY_REQUEST_TRIP_START_DATE         = "sdate"
let KEY_REQUEST_TRIP_END_DATE           = "edate"

// TRIP MAP
let KEY_REQUEST_TRIP_MAP_START_TS       = "start_timestamp"
let KEY_REQUEST_TRIP_MAP_END_TS         = "end_timestamp"

// NEWSFEED
let KEY_REQUEST_SUB_USERID              = "subuserId"


// NOTIFICATION
let KEY_REQUEST_NOTIFICATION_ID         = "notificationId"
let KEY_REQUEST_DEVICE_TARGET           = "deviceTarget"
let KEY_REQUEST_NOTIFICATION_STATE      = "state"

// RESPONSE

// Login
let KEY_RESPONSE_RESULT                 = "result"
let KEY_RESPONSE_COUNTRIES              = "countries"
let KEY_RESPONSE_COUNTRY_NAME           = "name"
let KEY_RESPONSE_USER_ID                = "user_id"
let KEY_RESPONSE_CLIENT_USER_ID         = "client_user_id"
let KEY_RESPONSE_USER_NAME              = "user_name"
let KEY_RESPONSE_SERVER_DOMAIN          = "server_domain"

// Settings
let KEY_RESPONSE_MAP_SERVERS            = "mapServers"
let KEY_RESPONSE_MAP_URL                = "url"
let KEY_RESPONSE_MAP_TITLE              = "label"
let KEY_RESPONSE_MAP_MINZOOM            = "minZoom"
let KEY_RESPONSE_MAP_MAXZOOM            = "maxZoom"
let KEY_RESPONSE_MAP_NICEZOOM           = "niceZoom"

// Client Data Limit
let KEY_RESPONSE_TRIP_LIMIT_LIST        = "clienttriplimitlist"
let KEY_RESPONSE_DATA_LIMIT_MONTHS      = "months"
let KEY_RESPONSE_DATA_LIMIT             = "client_data_limit"


// FLeet List
let KEY_RESPONSE_FLEET_GROUP_LIST       = "vehiclegroup.grouplist"
let KEY_RESPONSE_FLEET_GROUP_MATCH      = "vehiclegroup.groupvehicles"
let KEY_RESPONSE_FLEET_GROUP_VEHICLE_ID = "group_vehicle_id"
let KEY_RESPONSE_FLEET_NAME             = "name"
let KEY_RESPONSE_FLEET_LIST             = "fleetlist"
let KEY_RESPONSE_FLEET_VEHICLE_ID       = "vehicle_id"

// TRIP LIST
let KEY_RESPONSE_TRIP_DISTANCE          = "trip_distance"
let KEY_RESPONSE_TRIP_TIME              = "trip_time"
let KEY_RESPONSE_TRIP_DRIVER_NAME       = "driver_name"
let KEY_RESPONSE_TRIP_START_LOCATION    = "start_location"
let KEY_RESPONSE_TRIP_END_LOCATION      = "end_location"

// TRIP MAP
let KEY_RESPONSE_TRIP_MAP_START_TS       = "start_timestamp"
let KEY_RESPONSE_TRIP_MAP_END_TS         = "end_timestamp"

let KEY_REQUEST_COUNTRY_SELECTED_URL    = "selected_country_url"

// ERROR Codes

let ERROR_CODE_USER_CANCELLED           = -999
let KEY_RESPONSE_ERRORS                 = "error"
let KEY_RESPONSE_ERROR_MESSAGE          = "msg"







