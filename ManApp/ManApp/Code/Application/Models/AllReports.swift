

import Foundation

struct AllReports
{
    var strKey : String
    var strValue : String
    
    init(key: String, val: String) {
        self.strKey = key
        self.strValue = val
    }
}
