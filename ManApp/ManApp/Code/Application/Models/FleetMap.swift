//
//  FleetMap.swift
//  MiniMaps
//
//  Created by Siboniso Magwaza on 2015/12/21.
//  Copyright © 2015 Mobile Cartrack. All rights reserved.
//

import Foundation

class  FleetMap : NSObject  {
    var vehicleId : String?
    var registration : String?
    var odometer : String?
    var time : String?
    var fullDate : Date?
    var location : String?
    var ignition : Int?
    var latitude : Double?
    var longitude : Double?
    var speed : Double?
    var bearing : Double?
    var showOnMap : Bool = true // this is used for filtering vehicles
    var traffic_light : String?
    var vehicle_description : String?
    var generated_description : String?
    var client_driver_description : String?
    var position_description : String?
    var vehicle_geofence : String?
    var client_vehicle_description : String?
    var hasEmergency:Bool = false
    
    override init() {
        
    }
    
    init(param : NSDictionary) {
        self.vehicleId      = (param.value(forKey:KEY_RESPONSE_FLEET_VEHICLE_ID) as AnyObject).stringValue
        self.registration   = param.value(forKey:"registration") as? String
        self.odometer       = (param.value(forKey:"odometer") as AnyObject).stringValue
        self.time           = param.value(forKey:"event_ts") as? String
        self.fullDate = Utility.dateFromStringDate(self.time! as NSString, stringDateFormate: DATE_FORMAT_API)
        self.location       = param.value(forKey:"position_description") as? String
        self.ignition       = (param.value(forKey:"ignition") as AnyObject).integerValue
        self.latitude       = (param.value(forKey:"latitude")! as AnyObject).doubleValue
        self.longitude      = (param.value(forKey:"longitude")! as AnyObject).doubleValue
        self.speed          = (param.value(forKey:"speed")! as AnyObject).doubleValue
        self.bearing        = (param.value(forKey:"bearing")! as AnyObject).doubleValue
        self.traffic_light  = param.value(forKey:"vehicle_traffic_light") as? String
        self.vehicle_description            = param.value(forKey:"vehicle_description") as? String
        self.client_driver_description      = param.value(forKey:"client_driver_description") as? String
        self.position_description           = param.value(forKey:"position_description") as? String
        self.vehicle_geofence               = param.value(forKey:"vehicle_geofence") as? String
        self.client_vehicle_description     = param.value(forKey:"client_vehicle_description") as? String
        
    }

}
