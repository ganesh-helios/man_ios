

import Foundation

struct Alert
{
    var event_ts : String?
    var group_description : String?
    var notification_contact : String?
    var notification_msg   : String?
    var registration : String?
    var state_colour : String?
    var status : Int?
    var status_description : String?
    var trigger_description : String?
    var trigger_description2 : String?
    var event_ts_interval : TimeInterval?
    var generated_subtitle : String?
    
    
    init(param : NSDictionary)
    {
        self.event_ts               = param.value(forKey: "event_ts") as? String
        self.group_description      = param.value(forKey: "group_description") as? String
        self.notification_contact   = param.value(forKey:"notification_contact") as? String
        self.notification_msg       = param.value(forKey:"notification_msg") as? String
        self.registration           = param.value(forKey:"registration") as? String
        self.state_colour           = param.value(forKey:"state_colour") as? String
        self.status                 = (param.value(forKey:"status") as? Int)!
        self.status_description     = param.value(forKey:"status_description") as? String
        self.trigger_description    = param.value(forKey:"trigger_description") as? String
        self.trigger_description2   = param.value(forKey:"trigger_description2") as? String
        self.event_ts_interval      = Utility.dateFromStringAPIDate(self.event_ts!).timeIntervalSince1970
        self.generated_subtitle     = "";
        
    }
    
}
