//
//  MapTiles.swift
//  MiniMaps
//
//  Created by Siboniso Magwaza on 2015/12/21.
//  Copyright © 2015 Mobile Cartrack. All rights reserved.
//

import Foundation

struct MapTiles {
    var label : String?
    var url : String?
    var minZoom : Int?
    var maxZoom : Int?
    var niceZoom : Int?
    
}
