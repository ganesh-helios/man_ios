//
//  Trip.swift
//  MiniMaps
//
//  Created by Siboniso Magwaza on 2015/12/19.
//  Copyright (c) 2015 Cartrack. All rights reserved.
//

import Foundation

struct TripPoints{
    var vehicleId : String?
    var odometer : NSNumber?
    var time : String?
    var location : String?
    var rpm : String?
    var ignition : Int?
    var latitude : Double?
    var longitude : Double?
    var speed : Double?
    var bearing : Double?
    var roadSpeed:Double?
    init(param : NSDictionary){
        var latitude:CLLocationDegrees = 0
        var longitude:CLLocationDegrees = 0
        var speed:Double = 0
        var roadSpeed:Double = 0
        var time:String = ""
        var bearing:Double = 0

        if let lat:CLLocationDegrees = (param.value(forKey:"latitude") as AnyObject).doubleValue {
            latitude = lat
        }
        
        if let lon:CLLocationDegrees = (param.value(forKey:"longitude") as AnyObject).doubleValue {
            longitude = lon
        }
        
        if let vspeed:Double = (param.value(forKey:"speed")! as AnyObject).doubleValue {
            speed = vspeed
        }
        
        if let rspeed:Double = (param.value(forKey:"road_speed")! as AnyObject).doubleValue {
            roadSpeed = rspeed
        }
        if let vtime:String = param.value(forKey: "recieved_ts") as? String {
            time = vtime
        }
        
        if let vbearing:Double = (param.value(forKey:"bearing")! as AnyObject).doubleValue {
            bearing = vbearing
        }
        

        self.vehicleId      = (param.value(forKey:KEY_RESPONSE_FLEET_VEHICLE_ID) as AnyObject).stringValue
        self.odometer       = (param.value(forKey:"odometer") as! NSNumber)
        self.time           = time
        self.location       = param.value(forKey:"position_description") as? String
        self.rpm            = (param.value(forKey:"rpm") as AnyObject).stringValue
            self.ignition       = (param.value(forKey:"ignition") as AnyObject).integerValue
        self.latitude       = latitude
        self.longitude      = longitude
        self.speed          = speed
        self.bearing        = bearing
        self.roadSpeed      = roadSpeed
    }

}
