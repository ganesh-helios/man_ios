//
//  MapMarker.swift
//  MiniMaps
//
//  Created by Siboniso Magwaza on 2015/12/21.
//  Copyright © 2015 Mobile Cartrack. All rights reserved.
//


import Foundation
import MapKit

class MapMarker: MKPointAnnotation {
    var vehicleId:String!
    var registration : String!
    var ignition:Int!
    var imageName: String!
    var bearing: Double!
    var location: String!
    var odometer: String!
    var speed:Double!
    var time:String!

}
