//
//  Reports.swift
//  CarTrackFleet
//
//  Created by Ganesh on 28/12/15.
//  Copyright © 2015 Jigar Shah. All rights reserved.
//

import Foundation

struct Reports
{
    var allow_all_vehicles : Int?
    var allow_vehicle_groups : Int?
    var button_fields : String?
    var data_limits   : String?
    var label_fields : String
    var maximum_date_range : Int?
    var mobile_report_id : Int
    var parameter_fields : String
    var report_description : String
    var report_icon : String
    var report_name : String
    var result_fields : String
    var show_result_as_list : Int
    var unit_fields : String
    
    
    init(param : NSDictionary)
    {
        self.allow_all_vehicles     = param.value(forKey:"allow_all_vehicles") as? Int
        self.allow_vehicle_groups   = param.value(forKey:"allow_vehicle_groups") as? Int
        self.button_fields          = param.value(forKey:"button_fields") as? String
        self.data_limits            = param.value(forKey:"data_limits") as? String
        self.label_fields           = (param.value(forKey:"label_fields") as? String)!
        self.maximum_date_range     = Int(param.value(forKey:"maximum_date_range") as! String)!
        self.mobile_report_id       = (param.value(forKey:"mobile_report_id") as? Int)!
        self.parameter_fields       = (param.value(forKey:"parameter_fields") as? String)!
        self.report_description     = (param.value(forKey:"report_description") as? String)!
        self.report_icon            = param.value(forKey:"report_icon") as! String
        self.report_name            = param.value(forKey:"report_name") as! String
        self.result_fields          = param.value(forKey:"result_fields") as! String
        self.show_result_as_list    = param.value(forKey:"show_result_as_list") as! Int
        self.unit_fields            = param.value(forKey:"unit_fields") as! String
        
    }
    
}
