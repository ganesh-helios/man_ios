//
//  Date.swift
//  KinderKastle
//
//  Created by Diptesh Patel on 07/04/17.
//  Copyright © 2017 KinderKastle. All rights reserved.
//

import UIKit

extension Date {
    
    struct Gregorian {
        static let calendar = Calendar(identifier: .gregorian)
    }
    
    var startOfDay: Date {
        return Calendar.current.startOfDay(for: self)
    }
    
    var endOfDay: Date {
        var components = DateComponents()
        components.day = 1
        components.second = -1
        return Calendar.current.date(byAdding: components, to: startOfDay)!
    }
    
    var startOfWeek: Date {
        
        let calendar = Calendar.current
        var dateComponents = calendar.dateComponents([.year , .month , .weekOfMonth], from: self)
        dateComponents.weekday = 2
        dateComponents.timeZone = TimeZone.init(secondsFromGMT: 0)
        dateComponents.weekOfMonth = dateComponents.weekOfMonth!
        return calendar.date(from: dateComponents)!
    }
    
    var endOfWeek: Date {
        return Calendar.current.date(byAdding: .day, value: 6, to: self.startOfWeek)!
    }
    
    func getDayNTime() -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = DATE_FORMAT_DAY_TIME
        dateFormatter.locale =  Locale(identifier: "en_US_POSIX")
        dateFormatter.timeZone = TimeZone.init(secondsFromGMT: 0)
        return dateFormatter.string(from: self)
    }
    
    var actualStartOfWeek: Date? {
        return Gregorian.calendar.date(from: Gregorian.calendar.dateComponents([.yearForWeekOfYear, .weekOfYear], from: self))
    }
    var mondayStartOfTheWeek: Date {
        var calendar = Gregorian.calendar
        calendar.firstWeekday = 1
        var dateComponents = calendar.dateComponents([.year , .month , .weekOfMonth], from: self)
        
        dateComponents.weekday = 1
        return calendar.date(from: dateComponents)!
    }
    
    func getStringfromDate(_ stringFormat:String) -> String {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = stringFormat
        dateFormatter.locale =  Locale(identifier: "en_US_POSIX")
        dateFormatter.timeZone = TimeZone.init(secondsFromGMT: 0)
        return dateFormatter.string(from: self)
    }

}
