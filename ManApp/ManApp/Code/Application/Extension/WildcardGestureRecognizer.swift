//
//  WildcardGestureRecognizer.swift
//  
//
//  Created by Taras Kalapun on 25/07/14.
//  Copyright (c) 2014 Kalapun. All rights reserved.
//

import UIKit
//import UIKit.UIGestureRecognizerSubclass

typealias WildcardGestureRecognizerHandler = ( _ touches: NSSet, _ event: UIEvent, _ userTouch:Bool) -> ()

class WildcardGestureRecognizer: UIGestureRecognizer {
    
    var handler:WildcardGestureRecognizerHandler?
    
    init(target: AnyObject?!, action: Selector) {
        super.init(target: target, action: action)
        self.cancelsTouchesInView = false
    }
    
    
    init(handler: @escaping WildcardGestureRecognizerHandler) {
        super.init(target: nil, action: nil)
        self.handler = handler
        self.cancelsTouchesInView = false
    }
    
    
    func touchesBegan(_ touches: NSSet, withEvent event: UIEvent) {
        // do your stuff
        self.handler?(touches, event, true)
    }
    
    func touchesCancelled(_ touches: NSSet!, withEvent event: UIEvent!) {
        self.handler?(touches, event, false)

    }
    
    func touchesEnded(_ touches: NSSet!, withEvent event: UIEvent!) {
        self.handler?(touches, event, false)

    }
    
    func touchesMoved(_ touches: NSSet!, withEvent event: UIEvent!)  {
        
    }
    
    override func reset() {
        
    }
    
    override func ignore(_ touch: UITouch, for event: UIEvent)  {
        
    }
    
    override func canBePrevented(by preventingGestureRecognizer: UIGestureRecognizer) -> Bool {
        return false
    }
    
    override func canPrevent(_ preventedGestureRecognizer: UIGestureRecognizer) -> Bool  {
        return false
    }

    
}
