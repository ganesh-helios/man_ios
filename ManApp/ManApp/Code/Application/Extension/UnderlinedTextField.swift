//
//  UnderlinedTextField.swift
//  Communicator
//
//  Created by Diptesh Patel on 27/06/16.
//  Copyright © 2016 KinderKastle. All rights reserved.
//

import UIKit

class UnderlinedTextField : UITextField {
    var underlineLayer:CALayer = CALayer()
    
    override func layoutSubviews() {
        super.layoutSubviews()
        let width = CGFloat(2.0)
        underlineLayer.frame = CGRect(x: 0, y: self.frame.size.height - width, width:  self.frame.size.width, height: self.frame.size.height)
    }
    
    override func draw(_ rect: CGRect) {
        
        let width = CGFloat(2.0)
        underlineLayer.borderColor = UIColor.darkGray.cgColor
        underlineLayer.frame = CGRect(x: 0, y: self.frame.size.height - width, width:  self.frame.size.width, height: self.frame.size.height)
        
        underlineLayer.borderWidth = width
        self.layer.addSublayer(underlineLayer)
        self.layer.masksToBounds = true
        self.keyboardType = .asciiCapable
    }
}
