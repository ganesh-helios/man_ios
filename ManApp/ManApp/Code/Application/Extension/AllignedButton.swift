//
//  UIButton.swift
//  Communicator
//
//  Created by Diptesh Patel on 27/06/16.
//  Copyright © 2016 KinderKastle. All rights reserved.
//

import UIKit

class AllignedButton: UIButton  {
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        
        let spacing: CGFloat = 10.0
        let imageSize: CGSize = self.imageView!.image!.size
        self.titleEdgeInsets = UIEdgeInsetsMake(0.0, -imageSize.width, -(imageSize.height + spacing), 0.0)
        let labelString = NSString(string: self.titleLabel!.text!)
        let titleSize = labelString.size(withAttributes: [NSAttributedStringKey.font: self.titleLabel!.font])
        self.imageEdgeInsets = UIEdgeInsetsMake(-(titleSize.height + spacing), 0.0, 0.0, -titleSize.width)
        let edgeOffset = abs(titleSize.height - imageSize.height) / 2.0;
        self.contentEdgeInsets = UIEdgeInsetsMake(edgeOffset, 0.0, edgeOffset, 0.0)
        self.titleLabel!.lineBreakMode = NSLineBreakMode.byWordWrapping
        self.titleLabel?.textAlignment = .center
        self.titleLabel!.numberOfLines = 0//if you want unlimited number of lines put 0

    }
}

class AllignedButtonHome: UIButton  {
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        
        let spacing: CGFloat = 10.0
        let imageSize: CGSize = self.imageView!.image!.size
        self.titleEdgeInsets = UIEdgeInsetsMake(0.0, -imageSize.width, -(imageSize.height + spacing), 0.0)
        let labelString = NSString(string: self.titleLabel!.text!)
        let titleSize = labelString.size(withAttributes: [NSAttributedStringKey.font: self.titleLabel!.font])
        self.imageEdgeInsets = UIEdgeInsetsMake(-(titleSize.height + spacing), 0.0, 0.0, -titleSize.width)
        let edgeOffset = abs(titleSize.height - imageSize.height) / 2.0;
        self.contentEdgeInsets = UIEdgeInsetsMake(edgeOffset, 0.0, edgeOffset, 0.0)
        self.titleLabel!.lineBreakMode = NSLineBreakMode.byWordWrapping
        self.titleLabel?.textAlignment = .center
        self.titleLabel!.numberOfLines = 0//if you want unlimited number of lines put 0
        
    }
}
