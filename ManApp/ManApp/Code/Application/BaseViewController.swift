

import UIKit
import MBProgressHUD

class BaseViewController: UIViewController {
    
    @IBOutlet var subReportContainerView: UIView!
    var alertController:UIAlertController?
    var isKeyboardVisible:Bool = false

    var appDelegate = UIApplication.shared.delegate as! AppDelegate
    var hud : MBProgressHUD!
    var forceHideHUD:Bool = false
    var HUD:MBProgressHUD!
    var pageIndex:Int = 0
    //var report_Type_ID:Int = 0
    @IBOutlet var cnSubContainerViewHeight: NSLayoutConstraint!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        self.setNeedsStatusBarAppearanceUpdate()
        self.isKeyboardVisible = false
        
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self)
    }

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func findFirstResponder(inView view: UIView) -> UIView? {
        for subView in view.subviews {
            if subView.isFirstResponder {
                return subView
            }
            
            if let recursiveSubView = self.findFirstResponder(inView: subView) {
                return recursiveSubView
            }
        }
        
        return nil
    }
    
    @IBAction func unwindToMenuViewController(_ segue:UIStoryboardSegue) {
        
        if self.navigationController != nil {
           _ = self.navigationController?.popViewController(animated: true)
        }
    }

    // ProgressHUD
    func showProgress() {
        if self.hud != nil {
            self.hud.show(animated: true)
            return
        }
        if(self.navigationController != nil) {
            self.hud = MBProgressHUD.showAdded(to: (self.navigationController?.view)!, animated: true)
        } else {
            self.hud = MBProgressHUD.showAdded(to: self.view , animated: true)
        }
    }
    
    func hideProgress() {
        if (self.hud != nil) {
            if(self.hud.isHidden == false) {
                self.hud.hide(animated: true)
                self.hud = nil
            }
        }
    }
    
    func showToast(_ message:String) {
        self.showToast(TR_APP_NAME, message: message)
    }
    
    func showToast(_ title:String , message:String) {
        HUD = MBProgressHUD.init(view: appDelegate.window!)
        appDelegate.window?.addSubview(HUD)

        HUD.mode = MBProgressHUDMode.customView
        
        HUD.label.text = title;
        HUD.detailsLabel.text = message;
        HUD.show(animated: true)
        HUD.hide(animated: true, afterDelay: 3)
//        HUD.backgroundColor = UIColor.blue
        HUD.bezelView.backgroundColor = UIColor.darkGray;
        HUD.label.textColor = UIColor.white
        HUD.detailsLabel.textColor = UIColor.white
        let tapGesture:UITapGestureRecognizer = UITapGestureRecognizer.init(target: self, action: #selector(BaseViewController.hudTapped))
        HUD.isUserInteractionEnabled = true
        HUD.addGestureRecognizer(tapGesture)
        
    }
    
    @objc func hudTapped() {
        if(HUD != nil){
            HUD.hide(animated: true)
            HUD = nil
        }
    }
    
    
    func showAlert(_ message:String) {
        self.showAlert(message, title: "")
    }
    
    func showAlert(_ message:String, title:String) {
        self.alertController = UIAlertController(title: title, message:
            message, preferredStyle: UIAlertControllerStyle.alert)
        self.alertController!.addAction(UIAlertAction(title: TR_BTN_OK, style: UIAlertActionStyle.default,handler: nil))
        
        self.present(self.alertController!, animated: true, completion: nil)
        
    }
    
    func showAlertWithComplitionSingleOption(_ message: String, handler:((UIAlertAction) -> Void)?) {
        self.alertController = UIAlertController(title: "", message:
            message, preferredStyle: UIAlertControllerStyle.alert)
        self.alertController!.addAction(UIAlertAction(title: TR_BTN_OK, style: UIAlertActionStyle.default,handler: handler))
        
        self.present(self.alertController!, animated: true, completion: nil)
        
    }
    
    func showAlertWithComplition(_ message: String, handler:((UIAlertAction) -> Void)?) {
        
        self.alertController = UIAlertController(title: "", message:
            message, preferredStyle: UIAlertControllerStyle.alert)
        
        self.alertController!.addAction(UIAlertAction(title: TR_BTN_OK, style: UIAlertActionStyle.default, handler:handler))
        self.alertController!.addAction(UIAlertAction(title: TR_CANCEL, style: UIAlertActionStyle.cancel, handler:handler))
        
        self.present(self.alertController!, animated: true, completion: nil)
    }
    
    func showAlertErrorWithCompletion(_ error:NSError , handler:((UIAlertAction) -> Void)?) {
        let message = TR_MSG_PLEASE_TRY_AGAIN
        let title = "Error"
        // Show connection message for timeout and network issue
        self.alertController = UIAlertController(title: title, message:
            message, preferredStyle: UIAlertControllerStyle.alert)
        self.alertController!.addAction(UIAlertAction(title: TR_BTN_OK, style: UIAlertActionStyle.default,handler: handler))
        
        self.present(self.alertController!, animated: true, completion: nil)
        
    }
    
    func showAlertErrorWithCompletion(error:NSError , handler:((UIAlertAction) -> Void)?) {
        let message = "TR_MSG_UNABLE_TO_CONNECT"
        let title = "TR_GENERAL_ERROR"
        // Show connection message for timeout and network issue
        self.alertController = UIAlertController(title: title, message:
            message, preferredStyle: UIAlertControllerStyle.alert)
        self.alertController!.addAction(UIAlertAction(title: TR_BTN_OK, style: UIAlertActionStyle.default,handler: handler))
        
        self.present(self.alertController!, animated: true, completion: nil)
        
    }
    
    func showAlertError(error:NSError)
    {
        self.showAlertErrorWithCompletion(error, handler: nil)
    }

	func addSubView(_ controller:BaseViewController) {
        let nav = APP_DELEGATE.window?.rootViewController
        
		nav?.addChildViewController(controller)
		nav!.view.addSubview(controller.view)
		
		controller.view.translatesAutoresizingMaskIntoConstraints = false
		
		nav?.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-0-[view]-0-|",
						  options: NSLayoutFormatOptions(rawValue: 0),
						  metrics: nil,
						  views: ["view": controller.view]));
		
		nav?.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-0-[view]-0-|",
						  options: NSLayoutFormatOptions(rawValue: 0),
						  metrics: nil,
						  views: ["view": controller.view]));
		
	}
    
    func addToolBar() -> UIToolbar {
        let toolbarDone = UIToolbar.init()
//        toolbarDone.isTranslucent = true
        toolbarDone.barTintColor = UIColor.init(hex: "303C49")
        toolbarDone.sizeToFit()
        let barBtnDone = UIBarButtonItem.init(barButtonSystemItem: UIBarButtonSystemItem.done,
                                              target: self, action: #selector(BaseViewController.doneButton_Clicked(_:)))
        let barBtnflexi = UIBarButtonItem.init(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: self, action: nil)
        toolbarDone.items = [barBtnflexi,barBtnDone]
        return toolbarDone
    }
    
    //MARK: - doneButton_Clicked
    @objc func doneButton_Clicked(_ sender:Any) {
        self.view.endEditing(true)
        
    }
    
    //MARK: - didRotate
    override func didRotate(from fromInterfaceOrientation: UIInterfaceOrientation) {
    }
}
