//
//  APIURL.swift
//  KinderKastle
//
//  Created by Diptesh Patel on 27/09/16.
//  Copyright © 2016 KinderKastle. All rights reserved.
//

import Foundation

//LessonPlan 
let LESSON_PLAN_API_URL = "api/lessonplan/schedule/getbydate"

//LessonPlanMaster
let LESSON_PLAN_MASTER_API_URL = "api/lessonplan/activity"

//RelationshipList

let RELATIONSHIP_LIST_API_URL = "api/Relationship"

//ProgramScheduleList

let PROGRAM_SCHEDULE_LIST_API_URL = "api/programschedule/getallwithprogram"

//ContactTypeList

let CONTACT_TYPE_LIST_API_URL = "api/lead/getallleadcontacttypes"


//InventoryTypeList

let INVENTORY_TYPE_LIST_API_URL = "api/inventory/itemtype"

//InventoryList

let INVENTORY_LIST_API_URL = "api/inventory/classroomorder/getallitems"

//Request InventoryList

let REQUESTED_INVENTORY_LIST_API_URL = "api/inventory/classroomorder/getallrequesteditems"

//submit classroom Inventory Request

let SUBMIT_CLASSROOM_INVENTORY_URL = "api/inventory/classroomorder/AddUpdateClassRoomOrderRequest"

//admin supply inventory request

let ADMIN_SUPPLY_INVENTORY_URL = "api/inventory/classroomorder/AddUpdateClassRoomOrderSupply"

//LeadList

let Lead_LIST_API_URL = "api/lead/getalllead"

// Lead add
let LEAD_ENTRY_ADD_URL = "api/lead/"


//LeadSync

let Lead_SYNC_API_URL = "api/synch/lead"

// Lead update
let LEAD_UPDATE_URL = "api/lead/update"

//Save Report Media

let SAVE_REPORT_MEDIA_API_URL = "api/media"

//Save Observation Report

let SAVE_OBSERVATION_REPORT_API_URL = "api/observationreport"

//Save Boo Boo Gram Report

let SAVE_BOO_BOO_GRAM_REPORT_API_URL = "api/booboogramreport"

//Save Accident Report

let SAVE_ACCIDENT_REPORT_API_URL = "api/accidentreport"

//Save Incident Report

let SAVE_INCIDENT_REPORT_API_URL = "api/incidentreport"

//LessonPlanUpdate
let UPDATE_LESSON_PLAN_API_URL = "api/lessonplan/schedule"

//copyTo
let UPADTE_LESSON_PLAN_ACTIVITY_COPYTO = "api/lessonplan/schedule/copyto"//"api/lessonplan/schedulelog/copyto"

//copyFrom
let UPADTE_LESSON_PLAN_ACTIVITY_COPYFROM = "api/lessonplan/schedule/copyfrom"//"api/lessonplan/schedulelog/copyfrom"

//Update Monthly Theme
let UPDATE_MONTHLY_THEME        = "api/lessonplan/monthlytheme"

//Update Weekly Theme 
let UPDATE_WEEKLY_DETAILS       = "api/lessonplan/weeklytopic"

//ADd New Lesson plan
let ADD_NEW_LESSON_PLAN         = "api/lessonplan/schedule"


