//
//  WebApiController.swift
//  KinderKastle
//
//  Created by Diptesh Patel on 01/03/16.
//  Copyright © 2016 KinderKastle. All rights reserved.
//

import Foundation
import Alamofire
import CocoaLumberjack

typealias SuccessResponse = (AnyObject?) -> Void
typealias FailureResponse = (NSError?) -> Void
typealias ResponseBlock = (URLRequest?, HTTPURLResponse?, Data?, NSError?) -> Void

class WebApiController {
    
    static let sharedInstance:WebApiController = {
        let shared = WebApiController()
        shared.listenForReachability()
        return shared
    }()
    
    var configuration = URLSessionConfiguration.default
    var manager:Alamofire.SessionManager = Alamofire.SessionManager()
    var user_id:String?
    let reachabilityManager = Alamofire.NetworkReachabilityManager.init()

    fileprivate init() {
        configuration = URLSessionConfiguration.default
        let cookies = HTTPCookieStorage.shared
        configuration.httpCookieStorage = cookies
        configuration.timeoutIntervalForRequest = 60
        configuration.allowsCellularAccess = true
        configuration.httpMaximumConnectionsPerHost = 4
        let serverTrustPolicies: [String: ServerTrustPolicy] = [
            "apps.cartrack.com": .disableEvaluation
        ]
        
        manager = Alamofire.SessionManager.init(configuration: configuration,
                                                delegate:SessionDelegate(),
                                                serverTrustPolicyManager: ServerTrustPolicyManager(policies: serverTrustPolicies))
    }
    
    //MARK: - listenForReachability
    func listenForReachability() {
        
        self.reachabilityManager?.listener = { status in
            DDLogVerbose("Network Status Changed: \(status)")
            
            switch status {
            case .notReachable,.unknown:
                //Show error state
//                SyncManager.sharedInstance.networkStatusChanged(status: false)
                break
            case .reachable(.ethernetOrWiFi),.reachable(.wwan):
                //Hide error state
//                SyncManager.sharedInstance.networkStatusChanged(status: true)
                break
            }
        }
		
        self.reachabilityManager?.startListening()
        
    }

    /*
    func autoLogin(_ payload:HttpPayload, request:DataRequest, onSuccess: @escaping SuccessResponse, onFailure: @escaping FailureResponse) {
        if payload.retry == 2 {
            let error:NSError = NSError.init(domain: "Session Expired", code: 1001, userInfo: [AnyHashable: Any]() as? [String : Any] as! [String : Any])
            onFailure(error)
//            APP_DELEGATE.forceLogout()
            return
        }
//        let appDelegate:AppDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
//        appDelegate.forceLogoutUser()
        for cookie in HTTPCookieStorage.shared.cookies! {
            HTTPCookieStorage.shared.deleteCookie(cookie)
        }

        URLCache.shared.removeAllCachedResponses()

        payload.retry = payload.retry + 1
        guard let loginDict:NSDictionary = KEY_LOGIN_RE_LOGIN_PARAM.defaultsValue() as? NSDictionary else {

            DDLogVerbose(String.init(format:"Login Params are nil"))

            let error:NSError = NSError.init(domain: "Session Expired", code: 1001, userInfo: [AnyHashable: Any]())
            onFailure(error)
            APP_DELEGATE.forceLogout()
            return
        }
        DDLogVerbose("Token Expired Re Logging user ...")

        LoginApiController.loginWithParams(loginDict, onSuccess: { (response:AnyObject?) in
            self.send(payload, request: request, onSuccess: onSuccess, onFailure: onFailure)
        }) { (error:NSError?) in
            onFailure(error)
        }
        
    }
 */
    @discardableResult
    func send(_ payload:HttpPayload, request:DataRequest, onSuccess: @escaping SuccessResponse, onFailure: @escaping FailureResponse) -> DataRequest {


        UIApplication.shared.isIdleTimerDisabled = true
//request.response(queue: DispatchQueue?, completionHandler: <#T##(DefaultDataResponse) -> Void#>)
        request.responseString { (response:DataResponse<String>) in
            UIApplication.shared.isIdleTimerDisabled = false

            #if DEBUG
                if request.request?.httpBody == nil {
                    DDLogVerbose("\nRequest \(request.request?.urlRequest)")
                } else {
                    let data = (request.request?.httpBody)! as Data
                    if data.count < 10000 {
//                        DDLogVerbose("\nRequest \(request.request?.urlRequest) \nParams \n\(NSString.init(data: (request.request?.httpBody)!, encoding: String.Encoding.utf8.rawValue))")
                        
                    }
                }
                
//                print("HEADERS : \(request.request?.allHTTPHeaderFields)")
                
            #endif

            
            switch(response.result) {
                
            case .success(_):
                let data:NSData = NSData.init(data: response.data!)
                
                
                let allHeaders = request.response?.allHeaderFields
                if let getCoockie : String = allHeaders!["Set-Cookie"] as? String {
                    Utility.userDefaultsSetValue(value: getCoockie as AnyObject, key: KEY_COOKIE)
                }
                
                
                do {
                    let JSON = try JSONSerialization.jsonObject(with: data as Data, options: JSONSerialization.ReadingOptions.allowFragments)
                    DDLogVerbose("____Response \(JSON)")
    
                    onSuccess(JSON as AnyObject?)
                    
                } catch {
                    if let returnData = String(data: data as Data, encoding: .utf8) {
                        if(returnData.lowercased().contains("success")){
                            onSuccess(returnData as AnyObject?)
                        }else{
                            let error = NSError.init(domain: "Json", code: 1001, userInfo: [NSObject : AnyObject]() as? [String : Any])
                            onFailure(error)
                            
                            DDLogVerbose("Json Error : \(String(describing: NSString.init(data:data as Data, encoding: String.Encoding.utf8.rawValue)))")
                        }
                        
                    } else {
                        let error = NSError.init(domain: "Json", code: 1001, userInfo: [NSObject : AnyObject]() as? [String : Any])
                        onFailure(error)
                        
                        DDLogVerbose("Json Error : \(String(describing: NSString.init(data:data as Data, encoding: String.Encoding.utf8.rawValue)))")
                    }
                    
                    let allHeaders = request.response?.allHeaderFields
                    if let getCoockie : String = allHeaders!["Set-Cookie"] as? String {
                        Utility.userDefaultsSetValue(value: getCoockie as AnyObject, key: KEY_COOKIE)
                    }
                    if (response.result.value != nil) {
                        DDLogVerbose("StatusCode \(String(describing: response.response?.statusCode))")
                    }
                    
                    
                    
                    
                }
                
                break
            case .failure(_) :
                
//                print("HEADERS RESPONSE : \(String(describing: request.response?.statusCode)) :: \(request.response?.allHeaderFields)")
                
                if (response.result.value != nil){
                    DDLogVerbose("StatusCode \(response.response?.statusCode)")
                }
                
                if(payload.retry >= 10) {
                    onFailure(response.result.error as NSError?)
                    return
                }
                let error = response.result.error as! NSError
                
                if (error.code == -1005 || error.code == -1001 && payload.retry < 1) {
                    DDLogVerbose("Retrying \(error)")
                    
                    payload.retry += 1
                    self.send(payload, request:request,  onSuccess: onSuccess, onFailure: onFailure)
                    return
                }
                DDLogVerbose("Error \(error)")
                
                onFailure(error)
                break
                
            }
        }

        return request
    }
    
    @discardableResult
    func sendURLEncodeBody(_ payload:HttpPayload, onSuccess: @escaping SuccessResponse, onFailure: @escaping FailureResponse) -> Request {
        
//        let method =
        
        let request = manager.request(payload.url,
                                      method:  payload.method,
                                      parameters: payload.paramDict as? [String: Any],
                                      encoding: URLEncoding.default,
                                      headers: payload.headers)

        
        self.send(payload, request: request, onSuccess: onSuccess, onFailure: onFailure)
        return request
    }
    
    @discardableResult
    func sendJsonEncodeBody(_ payload:HttpPayload, onSuccess: @escaping SuccessResponse, onFailure: @escaping FailureResponse) -> Request {
        
        let request = manager.request(payload.url,
                                      method: payload.method,
                                      parameters: payload.paramDict as? Parameters,
                                      encoding: JSONEncoding.default,
                                      headers: payload.headers)
        
        self.send(payload, request: request, onSuccess: onSuccess, onFailure: onFailure)
        return request
    }
    
    //______________API_____________
    //MARK:-----------------
    
    //MARK: Get Country
    func getCountryList(_ paramDict:NSDictionary, onSuccess: @escaping SuccessResponse, onFailure: @escaping FailureResponse) -> DataRequest {
        let cookieStorage:HTTPCookieStorage = configuration.httpCookieStorage!
        for cookie in cookieStorage.cookies!
        {
            cookieStorage.deleteCookie(cookie)
        }
        
        Utility.userDefaultsSetValue(value: "" as AnyObject?, key: KEY_SERVER_URL)
        
        let URLStr = "countries.json"
        
        let payload:HttpPayload = HttpPayload.getPayloadWithUrlParam(URLStr as String, paramDict:paramDict)
        return sendURLEncodeBody(payload, onSuccess: onSuccess, onFailure: onFailure) as! DataRequest
    }
    
    //MARK: Login
    func loginWithEmailPassword(paramDict:NSDictionary, onSuccess: @escaping SuccessResponse, onFailure:@escaping FailureResponse) -> Void {
        
        var theJSONText:NSString = ""
        
        do{
            let theJSONData = try JSONSerialization.data(withJSONObject: paramDict, options: JSONSerialization.WritingOptions.init(rawValue: 0))
            theJSONText = NSString(data: theJSONData, encoding: String.Encoding.ascii.rawValue)!
        }
        catch let error as NSError {
            print(error)
        } catch {
            print("Unkown error")
        }
        let dataToSend:[String:String] = ["data":theJSONText as String]
        let URLStr = "service/login.php"
        
        let payload:HttpPayload = HttpPayload.postPayloadWithUrlParam(URLStr, paramDict: dataToSend as NSDictionary)
        print(payload)
        print(payload.paramDict)
        self.sendURLEncodeBody(payload, onSuccess: onSuccess, onFailure: onFailure)
    }
    
    //MARK: Forgot Password
    func forgotPasswordAPI(paramDict:NSDictionary, onSuccess: @escaping SuccessResponse, onFailure:@escaping FailureResponse) -> Void {
        
        let URLStr = "service/Functions.php?method=sendForgotEmail"
        let payload:HttpPayload = HttpPayload.getForgotPassPayloadWithUrlParam(URLStr, paramDict: paramDict as NSDictionary)
        self.sendURLEncodeBody(payload, onSuccess: onSuccess, onFailure: onFailure)
    }
    
    //MARK: Get Fleet List
    func getFleetList(paramDict:NSDictionary, onSuccess: @escaping SuccessResponse, onFailure:@escaping FailureResponse) -> Request {
        let user_id:NSString = (paramDict.value(forKey: KEY_REQUEST_USER_ID) as! NSString?)!
        let URLStr = String(format: "service/svc2.php/clients/%@/mobile/fleetlist",user_id)
        let payload:HttpPayload = HttpPayload.postPayloadWithUrlParam(URLStr, paramDict: [String: String]() as NSDictionary)
        
        return sendURLEncodeBody(payload, onSuccess: onSuccess, onFailure: onFailure)
        
    }
    
    //MARK: Get Trip List
    func getTripList(_ paramDict:NSDictionary, onSuccess: @escaping SuccessResponse, onFailure:@escaping FailureResponse) -> Request {
        let user_id:String = (paramDict.value(forKey: KEY_REQUEST_USER_ID) as! String?)!
        let vehicle_id:String = (paramDict.value(forKey: KEY_REQUEST_VEHICLE_ID) as! String?)!
        let sdate:String = (paramDict.value(forKey: KEY_REQUEST_TRIP_START_DATE) as! String?)!
        let edate:String = (paramDict.value(forKey: KEY_REQUEST_TRIP_END_DATE) as! String?)!
        
        let URLStr = String(format: "service/svc2.php/clients/%@/vehicles/%@/trips/datelist/sdate/%@/edate/%@",user_id,vehicle_id,sdate,edate)
        print(URLStr)
        let payload:HttpPayload = HttpPayload.postPayloadWithUrlParam(URLStr, paramDict: [String: String]() as NSDictionary)
        
        return sendURLEncodeBody(payload, onSuccess: onSuccess, onFailure: onFailure)
        
    }
    
    //MARK: Get Vehicle Trip
    func getVehicleTrip(_ paramDict:NSDictionary, onSuccess:@escaping SuccessResponse, onFailure:@escaping FailureResponse) -> Void {
        
        //service/svc2.php/clients/10125/vehicles/40705238/trips/select/d1/2015-05-12%2015:17:46%2B02/d2/2015-05-12%2015:42:08%2B02
        let user_id:NSString = (paramDict.value(forKey: KEY_REQUEST_USER_ID) as! NSString?)!
        let vehicle_id:NSString = (paramDict.value(forKey: KEY_REQUEST_VEHICLE_ID) as! NSString?)!
        var startTime:NSString = (paramDict.value(forKey: KEY_REQUEST_TRIP_MAP_START_TS) as! NSString?)!
        var endTime:NSString = (paramDict.value(forKey: KEY_REQUEST_TRIP_MAP_END_TS) as! NSString?)!
        
        startTime = startTime.addingPercentEncoding(withAllowedCharacters:.urlHostAllowed)! as NSString
        //endTime = endTime.addingPercentEscapes(using: String.Encoding.utf8.rawValue)! as NSString
        endTime = endTime.addingPercentEncoding(withAllowedCharacters:.urlHostAllowed)! as NSString
        
        let URLStr = String(format: "service/svc2.php/clients/%@/vehicles/%@/trips/select/d1/%@/d2/%@",user_id,vehicle_id,startTime,endTime)
        
        let payload:HttpPayload = HttpPayload.postPayloadWithUrlParam(URLStr, paramDict: [String: String]() as NSDictionary)
        
        sendURLEncodeBody(payload, onSuccess: onSuccess, onFailure: onFailure)
        
    }
    
    //MARK: Get Current Vehicle Trip
    func getCurrentVehicleTrip(_ paramDict:NSDictionary, onSuccess:@escaping SuccessResponse, onFailure:@escaping FailureResponse) -> Request {
        
        //service/svc2.php/clients/10125/vehicles/40705238/trips/select/d1/2015-05-12%2015:17:46%2B02/d2/2015-05-12%2015:42:08%2B02
        let user_id:NSString = (paramDict.value(forKey: KEY_REQUEST_USER_ID) as! NSString?)!
        let vehicle_id:NSString = (paramDict.value(forKey: KEY_REQUEST_VEHICLE_ID) as! NSString?)!
        
        let URLStr = String(format: "service/svc2.php/clients/%@/vehicles/%@/trips/current",user_id,vehicle_id)
        
        let payload:HttpPayload = HttpPayload.postPayloadWithUrlParam(URLStr, paramDict: [String: String]() as NSDictionary)
        
        return sendURLEncodeBody(payload, onSuccess: onSuccess, onFailure: onFailure)
        
    }
    
    //MARK: Get Client Data limit
    func getClientDataLimit(_ paramDict:NSDictionary, onSuccess:@escaping SuccessResponse, onFailure:@escaping FailureResponse) -> Request {
        
        let user_id:NSString = (paramDict.value(forKey: KEY_REQUEST_USER_ID) as! NSString?)!
        
        let URLStr = String(format: "service/svc2.php/clients/%@/mobile/client_trip_limit",user_id)
        
        let payload:HttpPayload = HttpPayload.postPayloadWithUrlParam(URLStr, paramDict: [String: String]() as NSDictionary)
        
        return sendURLEncodeBody(payload, onSuccess: onSuccess, onFailure: onFailure)
        
    }
}
