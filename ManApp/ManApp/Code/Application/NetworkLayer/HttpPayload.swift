//
//  HttpPayload.swift
//  KinderKastle
//
//  Created by Diptesh Patel on 01/03/16.
//  Copyright © 2016 KinderKastle. All rights reserved.
//


/*
Apps hosted on Client environment:
API: http://kinderlinkapi-test.azurewebsites.net
Portal: http://kinderlinkportal-test.azurewebsites.net
*/

import Foundation
import Alamofire
//let baseURL = "https://apps.cartrack.com/eld"
//let baseURL = "https://fleet10.cartrack.co.za" // "https://fleetusa.cartrack.com/eldapp"
let baseURL = "https://apps.cartrack.com"
let forgotPassURL = "https://fleet5.cartrack.co.za"

open class HttpPayload : NSObject, NSCoding {
    
    
    struct HttpsKeys {
        static let KEY_URL          = "KEY_URL"
        static let KEY_METHOD       = "KEY_METHOD"
        static let KEY_PARAMDICT    = "KEY_PARAMDICT"
        static let KEY_RETRY        = "KEY_RETRY"
        static let KEY_HEADERS      = "KEY_HEADERS"
    }
    
    
    var url:String = ""
    var paramDict:NSDictionary = NSDictionary()
    var method:Alamofire.HTTPMethod = Alamofire.HTTPMethod.get
    var retry:Int = 0
    var headers:[String:String] = [:]//NSDictionary()
    
    override init() {
        //        super.init()
    }
    
    //MARK: - getPayloadWithUrlParam
    open class func getPayloadWithUrlParam(_ urlString:String, paramDict:NSDictionary)-> HttpPayload
    {
        var fullURL = Utility.userDefaultsGetStringValue(key: KEY_SERVER_URL)
        if (fullURL.length == 0) {
            fullURL = baseURL
        }
        
        let isCoockie = Utility.userDefaultsGetStringValue(key: KEY_COOKIE)
        var newHeaders : [String:String] = [:]
        if isCoockie.length > 0 {
            newHeaders = [KEY_COOKIE:isCoockie]
        }
        
        let httpPayload:HttpPayload = HttpPayload.init()
        httpPayload.method = Alamofire.HTTPMethod.get
        httpPayload.url = "\(fullURL)/\(urlString)"
        httpPayload.paramDict = paramDict
        httpPayload.headers = newHeaders
        
        return httpPayload
    }
    
    //MARK: - getForgotPassPayloadWithUrlParam
    open class func getForgotPassPayloadWithUrlParam(_ urlString:String, paramDict:NSDictionary)-> HttpPayload
    {
        let fullURL = forgotPassURL
        
        let isCoockie = Utility.userDefaultsGetStringValue(key: KEY_COOKIE)
        var newHeaders : [String:String] = [:]
        if isCoockie.length > 0 {
            newHeaders = [KEY_COOKIE:isCoockie]
        }
        
        let httpPayload:HttpPayload = HttpPayload.init()
        httpPayload.method = Alamofire.HTTPMethod.get
        httpPayload.url = "\(fullURL)/\(urlString)"
        httpPayload.paramDict = paramDict
        httpPayload.headers = newHeaders
        
        return httpPayload
    }
    
    //MARK: - postPayloadWithUrlParam
    open class func postPayloadWithUrlParam(_ urlString:String, paramDict:NSDictionary)-> HttpPayload
    {
        var fullURL = Utility.userDefaultsGetStringValue(key: KEY_SERVER_URL)
        if (fullURL.length == 0) {
            fullURL = baseURL
        }
        let isCoockie = Utility.userDefaultsGetStringValue(key: KEY_COOKIE)
        var newHeaders : [String:String] = [:]
        if isCoockie.length > 0 {
            newHeaders = [KEY_COOKIE:isCoockie]
        }
        
        let httpPayload:HttpPayload = HttpPayload.init()
        httpPayload.method = Alamofire.HTTPMethod.post
        httpPayload.url = "\(fullURL)/\(urlString)"// fullURL.appendingPathComponent(urlString as String) + "/"
        httpPayload.paramDict = paramDict
        httpPayload.headers = newHeaders
        
        return httpPayload
    }
    
    public required init?(coder aDecoder: NSCoder) {
        
        if let url_:String = aDecoder.decodeObject(forKey: HttpsKeys.KEY_URL) as? String {
            self.url = url_
        }
        
        if let param_:NSDictionary = aDecoder.decodeObject(forKey: HttpsKeys.KEY_PARAMDICT) as? NSDictionary {
            self.paramDict = param_
        }
        
        if let method_:Alamofire.HTTPMethod = aDecoder.decodeObject(forKey: HttpsKeys.KEY_METHOD) as? Alamofire.HTTPMethod {
            self.method = method_
        }
        
        if let val:Int = aDecoder.decodeObject(forKey: HttpsKeys.KEY_RETRY) as? Int{
            self.retry = val
        }
        
        if let param_:NSDictionary = aDecoder.decodeObject(forKey: HttpsKeys.KEY_HEADERS) as? NSDictionary {
            self.headers = param_ as! [String: String]
        }
        
    }
    
    public func encode(with aCoder: NSCoder) {
        print("\(self.method) dict = \(self.paramDict)")
        aCoder.encode(self.url, forKey: HttpsKeys.KEY_URL)
        //        aCoder.encode(self.method, forKey: HttpsKeys.KEY_METHOD)
        aCoder.encode(self.paramDict, forKey: HttpsKeys.KEY_PARAMDICT)
        aCoder.encode(self.retry, forKey: HttpsKeys.KEY_RETRY)
        aCoder.encode(self.headers, forKey: HttpsKeys.KEY_HEADERS)
    }
    
    
    
    
    
}


